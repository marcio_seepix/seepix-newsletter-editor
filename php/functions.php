<?php
if(!function_exists('email'))
	{
		function email($title,$msg,$email_receiver='')
			{

				$db = new db();
				$config_global_content = $db->load("Select cod, valor from config_global");
				$config_global = array();
				foreach ($config_global_content as $key => $value)
					{
						$cod = $value['cod'];
						$valor = $value['valor'];
						$config_global[$cod] = utf8_decode($valor);
					}

				$email_autenticado = $config_global['email_config_autenticado'];
				$email_porta = $config_global['email_porta'];
				$email_smtp = $config_global['email_smtp'];
				$email_tls = $config_global['email_config_tls'];
				$email_saida = $config_global['email_saida'];
				$email_senha = $config_global['email_senha'];

				if($email_receiver=='')
					{
						$email_receiver = $config_global['email_recebimento'];
					}

				if($email_autenticado)
					{
						if(class_exists('PHPMailer'))
							{

								$mailer = new PHPMailer();
								$mailer->IsSMTP();
								$mailer->SMTPDebug = 0;
								$mailer->Port = $email_porta; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.

								$mailer->Host = $email_smtp; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:

								if($email_tls){
									$mailer->SMTPSecure = 'tls';
									}

								$mailer->SMTPAuth 	= true; //Define se haverá ou não autenticação no SMTP
								$mailer->Username 	= $email_saida; //Informe o e-mai o completo
								$mailer->Password 	= $email_senha; //Senha da caixa postal
								$mailer->FromName 	= SYSTEM_TITTLE; //Nome que será exibido para o destinatário
								$mailer->From 		= $email_saida; //Obrigatório ser a mesma caixa postal indicada em "username"
								$mailer->AddAddress($email_receiver); //Destinatários
								$mailer->Subject 	= $title;
								$mailer->Body 		= $msg;
								$mailer->IsHTML(true);

								if(!$mailer->Send()){
									return false;
									} else {
									return true;
									}

							}
					} else {

						$headers = "MIME-Version: 1.1\n";
						$headers .= "Content-type: text/html; charset=iso-8859-1\n";
						$headers .= "From: ".$email_saida."<".$email_saida.">\n";
						$headers .= "Cc:\n";
						$headers .= "Bcc:\n";
						$headers .= "Reply-To: ".$email_saida."\n";

						$check = mail($email_receiver, $title, $msg, $headers);

					}


				if(!class_exists('PHPMailer'))
					{
						return false;
					}

			}
	}

/* ---------------------------------
Aplica o utf8_encode ao 
conteúdo de um array
--------------------------------- */
if(!function_exists('json_encode_utf8'))
	{
		function json_encode_utf8($dados)
			{

				foreach ($dados as $key => $value)
					{
						if(gettype($value)=='array')
							{
								$dados[$key] = json_encode_utf8($value);
							}
						if(gettype($value)=='string')
							{

								$dados[$key] = utf8_encode($value);
							}
					}

				return $dados;
			}		
	}

/* ---------------------------------
Recebe o idioma do usuário
--------------------------------- */
if(!function_exists('idiom'))
	{
		function idiom($cod)
			{

				$idiom_content = $GLOBALS['idiom_content'];
				if(!isset($idiom_content))
					{
						return false;
					}

				if(isset($idiom_content[$cod]))
					{
						return $idiom_content[$cod];
					} else {
						return false;
					}
			}
	}

/* ---------------------------------
Recebe variáveis enviadas por GET ou
POST com segurança
--------------------------------- */
if(!function_exists('get'))
	{
		function get($var)
			{

				$var = str_replace("´","'", $var);
				$var = str_replace("&", "&amp;", $var);
				$var = str_replace("\"", "&quot;", $var);
				$var = str_replace("'", "&#039;", $var);
				$var = str_replace("<", "&lt;", $var);
				$var = str_replace(">", "&gt;", $var);

				return $var;

			}
	}
?>
<?php
/* ---------------------------------
@version Beta v0.2
@date 30.Mai.2017
@author Marcio Camargo
@email marcio@seepix.com.br

Fun��es de processamento de imagens

require_once('class/img.class.php');

$copy_img = new img();
$copy_img -> dir_source('/adm/components/upload_html5/upload_files/');
$copy_img -> name_source($cod_upload);
$copy_img -> dir_dest($file_dir);
$copy_img -> name_dest($name_upload);
$copy_img -> check_dest_exists();
$copy_img -> set_w($upload_w);
$copy_img -> set_h($upload_h);
$copy_img -> resize();

--------------------------------- */

class img
	{

		public $dir_source	= '';
		public $dir_dest	= '';
		public $name_source	= '';
		public $name_dest	= '';
		public $w_dest		= 0;
		public $h_dest		= 0;

		public function __construct() {
	        
	        $rootDirectory = $_SERVER['DOCUMENT_ROOT'];
	        $this->rootDirectory = $rootDirectory;

	        global $serverDirectory;
	        $this->serverDirectory = $serverDirectory;

	        /* ---------------------------------
			Remove caracteres no in�cio da string
			--------------------------------- */
			if(!function_exists('char_remove_begin'))
				{
					function char_remove_begin($string,$char)
						{

							if($string==''){
								return false;
								}

							if($char==''){
								return false;
								}

							$string_char = substr($string, 0, 1);
							if($string_char=='/'){
								$string = substr($string, 1);
								}

							return $string;

						}
				}

			/* ---------------------------------
			Remove caracteres no final da string
			--------------------------------- */
			if(!function_exists('char_remove_end'))
				{
					function char_remove_end($string,$char)
						{

							if($string==''){
								return false;
								}

							if($char==''){
								return false;
								}

							$string_char = substr($string, -1);
							if($string_char==$char){
								$string = substr($string, 0, -1);
								}

							return $string;

						}
				}

	    	}

		/* --------------------------------------------------------------------------------------
		Define o diret�rio de origem da imagem
		-------------------------------------------------------------------------------------- */
		
			function dir_source($value){
				$this -> dir_source = $value;
				}

		/* --------------------------------------------------------------------------------------
		Define o diret�rio de destino fonte da imagem
		-------------------------------------------------------------------------------------- */
		
			function dir_dest($value){
				$this -> dir_dest = $value;
				}

		/* --------------------------------------------------------------------------------------
		Define o nome da imagem de origem
		-------------------------------------------------------------------------------------- */
		
			function name_source($value){
				$this -> name_source = $value;
				}

		/* --------------------------------------------------------------------------------------
		Define o nome da imagem de destino
		-------------------------------------------------------------------------------------- */
		
			function name_dest($value){
				$this -> name_dest = $value;
				}

		/* --------------------------------------------------------------------------------------
		Checa se os requerimentos para a c�pia
		-------------------------------------------------------------------------------------- */
			
			function check_requirements()
				{

					if(!isset($this -> rootDirectory))
						{
							print 'A vari�vel $rootDirectory n�o foi definida no config.';
							return false;
						}

					if(!isset($this -> serverDirectory))
						{
							print 'A vari�vel $serverDirectory n�o foi definida no config.';
							return false;
						}

					if($this->dir_source=="")
						{
							print 'A vari�vel $dir_source n�o foi definida.';
							return false;
						}

					if($this->name_source=="")
						{
							print 'A vari�vel $name_source n�o foi definida.';
							return false;
						}

					if($this->dir_dest=="")
						{
							print 'A vari�vel $dir_dest n�o foi definida.';
							return false;
						}

					if($this->name_dest=="")
						{
							print 'A vari�vel $name_dest n�o foi definida.';
							return false;
						}

				}		

		/* --------------------------------------------------------------------------------------
		Checa se o arquivo fonte existe no diret�rio
		-------------------------------------------------------------------------------------- */
		function check_source()
			{
			
				$url_source = $this -> url_source();
				if(file_exists($url_source))
					{
						return true;
					} else {
						return false;
					}
		
			}

		/* --------------------------------------------------------------------------------------
		Checa se o arquivo de destino existe no diret�rio
		-------------------------------------------------------------------------------------- */
		function check_dest()
			{

				$url_dest = $this -> url_dest();
				if(file_exists($url_dest))
					{
						return true;
					} else {
						return false;
					}

			}

		/* --------------------------------------------------------------------------------------
		Busca um nome de destino para caso j� exista
		-------------------------------------------------------------------------------------- */
		function check_dest_exists()
			{

				if($this -> check_source())
					{

						$name_dest = $this -> name_dest;
						$url_source = $this -> url_source();
						$url_dest = $this -> url_dest();
						$check_dest = $this -> check_dest();

						$new_name = $name_dest;
						$contar = 0;
						while($check_dest)
							{

								$new_name = $contar.'_'.$name_dest;
								$this -> name_dest($new_name);
								$url_dest = $this -> url_dest();
								$check_dest = $this -> check_dest();
								$contar++;

							}

						$this -> name_dest = $new_name;

					} else {
						return false;
					}

			}

		/* --------------------------------------------------------------------------------------
		Monta a url completa de origem
		-------------------------------------------------------------------------------------- */
		function url_source()
			{

				$this -> check_requirements();

				$rootDirectory = $this -> rootDirectory;
				$serverDirectory = $this -> serverDirectory;
				$dir_source = $this -> dir_source;
				$name_source = $this -> name_source;

				/* ---------------------------------
				Removendo a barra no final e final do $rootDirectory
				--------------------------------- */
				//$rootDirectory = char_remove_begin($rootDirectory,'/');
				$rootDirectory = char_remove_end($rootDirectory,'/');

				/* ---------------------------------
				Removendo a barra no final e final do $serverDirectory
				--------------------------------- */
				$serverDirectory = char_remove_begin($serverDirectory,'/');
				$serverDirectory = char_remove_end($serverDirectory,'/');

				/* ---------------------------------
				Removendo a barra no final e final do DIR_SOURCE
				--------------------------------- */
				$dir_source = char_remove_begin($dir_source,'/');
				$dir_source = char_remove_end($dir_source,'/');

				/* ---------------------------------
				Removendo a barra no in�cio do nome
				--------------------------------- */
				$name_source = str_replace("/","", $name_source);
				
				/* ---------------------------------
				Checando se o arquivo de origem existe
				--------------------------------- */
				$url_source = $rootDirectory.'/'.$serverDirectory.'/'.$dir_source.'/'.$name_source;
				return $url_source;

			}

		/* --------------------------------------------------------------------------------------
		Monta a url completa de destino
		-------------------------------------------------------------------------------------- */
		function url_dest()
			{
				
				$this -> check_requirements();

				$rootDirectory = $this -> rootDirectory;
				$serverDirectory = $this -> serverDirectory;
				$dir_dest = $this -> dir_dest;
				$name_dest = $this -> name_dest;

				/* ---------------------------------
				Removendo a barra no final e final do $rootDirectory
				--------------------------------- */
				//$rootDirectory = char_remove_begin($rootDirectory,'/');
				$rootDirectory = char_remove_end($rootDirectory,'/');

				/* ---------------------------------
				Removendo a barra no final e final do $serverDirectory
				--------------------------------- */
				$serverDirectory = char_remove_begin($serverDirectory,'/');
				$serverDirectory = char_remove_end($serverDirectory,'/');

				/* ---------------------------------
				Removendo a barra no final e final do DIR_SOURCE
				--------------------------------- */
				$dir_dest = char_remove_begin($dir_dest,'/');
				$dir_dest = char_remove_end($dir_dest,'/');

				/* ---------------------------------
				Removendo a barra no in�cio do nome
				--------------------------------- */
				$name_dest = str_replace("/","", $name_dest);
				
				/* ---------------------------------
				Checando se o arquivo de origem existe
				--------------------------------- */
				$url_dest = $rootDirectory.'/'.$serverDirectory.'/'.$dir_dest.'/'.$name_dest;
				return $url_dest;

			}

		/* --------------------------------------------------------------------------------------
		Setando o width de destino
		-------------------------------------------------------------------------------------- */
		function set_w($value)
			{
				$this -> w_dest = $value;
			}

		/* --------------------------------------------------------------------------------------
		Setando o height de destino
		-------------------------------------------------------------------------------------- */
		function set_h($value)
			{
				$this -> h_dest = $value;
			}

		/* --------------------------------------------------------------------------------------
		Redimensionar
		-------------------------------------------------------------------------------------- */
		function resize()
			{

				$this -> check_requirements();

				$dest_w = $this -> w_dest;
				$dest_h = $this -> h_dest;
				$url_source = $this -> url_source();
				$url_dest = $this -> url_dest();

				if($dest_w>0 && $dest_h>0){

						clearstatcache();

						$source_info = getimagesize($url_source); 
						list($source_w,$source_h, $type) = $source_info; 
						
						if($source_w==0){
							return false; 
							}

						if($type>3){
							return false; 
							}

						$source_prop = $source_h / $source_w; 
						$dest_prop = $dest_h / $dest_w;

						/* ---------------------------------
						Ajuste Horizontal
						--------------------------------- */
						if($source_prop>=$dest_prop)
							{

								$prop_diff 	= $dest_w/$source_w; 
								$final_h 	= $prop_diff*$source_h; 
								$final_w	= $dest_w;
								$cut_x 		= 0; 
								$cut_y 		= ($final_h - $dest_h)/2; 

							}

						if($source_prop<$dest_prop)
							{

								$prop_diff 	= $dest_h/$source_h; 
								$final_w 	= $prop_diff*$source_w; 
								$final_h	= $dest_h;
								$cut_y 		= 0; 
								$cut_x 		= ($dest_h- $final_h)/2; 

							}

						$final_pos_x = ($dest_w - $final_w)/2;
						$final_pos_y = ($dest_h - $final_h)/2;

						$final_pos_x = (integer)$final_pos_x;
						$final_pos_y = (integer)$final_pos_y;
						$cut_x = (integer)$cut_x;
						$cut_y = (integer)$cut_y;
						$final_w = (integer)$final_w;
						$final_h = (integer)$final_h;
						$source_w = (integer)$source_w;
						$source_h = (integer)$source_h;

						$source_set = ImageCreateFromjpeg($url_source); 	
						$final_img = imagecreatetruecolor($dest_w,$dest_h);

						imagecopyresampled(
								$final_img,
								$source_set,
								$final_pos_x,
								$final_pos_y,
								$cut_x,
								$cut_y,
								$final_w,
								$final_h,
								$source_w,
								$source_h
								);

						$final_img_save = $url_dest; 					
						ImageJPEG($final_img,$final_img_save, 80); 	
						ImageDestroy($final_img);

					} else {
						return false;
					}
			}

		/* --------------------------------------------------------------------------------------
		Copiando a imagem para o destino
		-------------------------------------------------------------------------------------- */
		function copy()
			{

				if($this -> check_source())
					{

						$url_source = $this -> url_source();
						$url_dest = $this -> url_dest();

						$check_copy = copy($url_source, $url_dest);

						if($check_copy){
							return true;
							} else {
							return false;	
							}

					} else {
						return false;
					}
			}
	}
/* --------------------------------------------------------------------------------------------
fim da classe IMG
-------------------------------------------------------------------------------------------- */
?>
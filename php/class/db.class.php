<?php
/* ---------------------------------
@version Beta v0.4
@date 10.Mai.2018
@author Marcio Camargo
@email marcio@seepix.com.br

Funções do Banco de dados
- Atualizado para segurança em injection
--------------------------------- */

if(!isset($mysqli)){
	$mysqli = new mysqli(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
	if ($mysqli->connect_errno) {
		    echo "Problemas na conexão ao Banco de Dados.";
		    die();
		}

	}

class db{

	public $mysqli 		= '';
	public $query 		= '';
	public $tab 		= '';
	public $fields 		= '*';
	public $where 		= '';
	public $order 		= '';
	public $error 		= '';

	public function __construct() {
        
        global $mysqli;
        $this->mysqli = $mysqli;
    	
    	}

	    function query_safe($query){

	    	$query = str_ireplace("drop table","", $query);
	    	$query = str_ireplace("OR 1=1","", $query);
	    	$query = str_ireplace("OR 1 = 1","", $query);
	    	$query = str_ireplace("or 1=1","", $query);
	    	$query = str_ireplace("or 1 =1","", $query);
	    	$query = str_ireplace("or 1= 1","", $query);
	    	$query = str_ireplace("or 1 = 1","", $query);
	    	$query = str_ireplace("or '1'='1'","", $query);
	    	$query = str_ireplace("or (1=1)","", $query);
	    	$query = str_ireplace("\" or \"\"=\"","", $query);
	    	$query = str_ireplace("Drop TABLE","", $query);
	    	$query = str_ireplace("DROP TABLE","", $query);
	    	$query = str_ireplace("CREATE TABLE","", $query);	
	    	$query = str_ireplace("RENAME TABLE","", $query);
	    	$query = str_ireplace("TRUNCATE TABLE","", $query);
	    	$query = str_ireplace("REPLACE TABLE","", $query);
	    	
	    	// $query_array_check = str_word_count($query,1);

	    	// $query_array_check[0] = strtolower($query_array_check[0]);

	    	// if($query_array_check[0]=='select')
	    	// 	{
	    	// 		$query = strtolower($query);
	    	// 		$query = preg_replace('/\s+/', ' ',$query);

	    	// 		$query = str_ireplace("delete ","", $query);
	    	// 		$query = str_ireplace("update ","", $query);

	    	// 	}

	    	return $query;

	    	}

	/* --------------------------------------------------------------------------------------
	define a SQL de busca 
	-------------------------------------------------------------------------------------- */
	
		function query($value){
			$this -> query = $value;
			}

	/* --------------------------------------------------------------------------------------
	define a tabela de busca 
	-------------------------------------------------------------------------------------- */
	
		function tab($value){
			$this -> tab = $value;
			}

	/* --------------------------------------------------------------------------------------
	define os campos da tabela na busca 
	-------------------------------------------------------------------------------------- */
	
		function fields($value){
			$this -> fields = $value;
			}

	/* --------------------------------------------------------------------------------------
	define o critério da sql
	-------------------------------------------------------------------------------------- */
	
		function where($value){
			$this -> where = $value;
			}

	/* --------------------------------------------------------------------------------------
	define a ordem da sql
	-------------------------------------------------------------------------------------- */
	
		function order($value){
			$this -> order = $value;
			}

	/* --------------------------------------------------------------------------------------
	Conta os registros de tabela
	-------------------------------------------------------------------------------------- */
	
		function num_rows($query=''){

			$retorno = 0;

			$mysqli = $this->mysqli;
			if($query=='')
				{
					$query = $this -> query_mount();		
				}

			$query = $this->query_safe($query);

			$result = $mysqli->query($query);
			if($result -> num_rows)
				{
					$retorno = $result -> num_rows;
				}

			// $result->close();

			return $retorno;

			}

	/* --------------------------------------------------------------------------------------
	Checa se um campo existe no BD
	-------------------------------------------------------------------------------------- */

		function check_field($tab,$field){

			$mysqli = $this->mysqli;
			$query = "SELECT * from $tab limit 0,1";
			$return = 0;

			if($result = $mysqli->query($query))
				{

					$finfo = $result->fetch_fields();
					foreach ($finfo as $key => $value) 
						{
						
							$field_check = $value->name;
							if($field_check==$field)
								{
									$return = 1;
								}

						}

				}

			// $result->close();
			return $return;

			}

	/* --------------------------------------------------------------------------------------
	Retorna um array com os campos da tabela
	-------------------------------------------------------------------------------------- */

		function load_fields($tab){

			$mysqli = $this->mysqli;
			$query = "SELECT * from $tab limit 0,1";
			$array_fields = array();

			if($result = $mysqli->query($query))
				{

					$finfo = $result->fetch_fields();
					foreach ($finfo as $key => $value) 
						{
						
							$field = $value->name;
							array_push($array_fields, $field);
							
						}

				}

			// $result->close();

			if(sizeof($array_fields)==0)
				{
					return false;		
				}

			if(sizeof($array_fields)>0)
				{
					return $array_fields;
				}
			

			}

	/* --------------------------------------------------------------------------------------
	Retorna um array com as informações sobre campos da tabela
	-------------------------------------------------------------------------------------- */

		function load_fields_info($tab){

			$mysqli = $this->mysqli;
			$query = "SELECT * from $tab limit 0,1";
			$array_fields = array();

			if($result = $mysqli->query($query))
				{


					$finfo = $result->fetch_fields();
					foreach ($finfo as $key => $value) 
						{
							
							$field['name'] = $value->name;
							$field['table'] = $value->table;
							$field['max_length'] = $value->max_length;
							$field['flags'] = $value->flags;
							$field['type'] = $value->type;

							array_push($array_fields, $field);
							
						}
				}

			// $result->close();

			if(sizeof($array_fields)==0)
				{
					return false;		
				}

			if(sizeof($array_fields)>0)
				{
					return $array_fields;
				}
			

			}

	/* --------------------------------------------------------------------------------------
	Monta a query
	-------------------------------------------------------------------------------------- */

		function query_mount(){

			$query 	= $this -> query;
			$fields = $this -> fields;
			$tab 	= $this -> tab;
			$where 	= $this -> where;
			$order 	= $this -> order;

			if($query!='')
				{
					
					return $query;

				} else {

					$query = 'Select ';
					$query.= $fields;
					$query.= ' from ';
					$query.= $tab;

					if($where!=''){
						$query.= ' where '.$where;						
						}

					if($order!=''){
						$query.= ' order by '.$order;
						}

					return $query;

				}

			}

	/* --------------------------------------------------------------------------------------
	Executa a busca no banco de dados 
	-------------------------------------------------------------------------------------- */

		function load($query=''){

			if($query=='')
				{
					$query = $this -> query_mount();		
				}
			
			$mysqli = $this->mysqli;
			$return = array();

			$query = $this->query_safe($query);

			if($result = $mysqli->query($query)) 
				{
			    
				    while ($row = $result->fetch_assoc())
				    	{
				        	array_push($return, $row);
				    	}
				    
				    // $result->close();
			
				}

			// $result->close();

			return $return;

			}

	/* --------------------------------------------------------------------------------------
	Retorna um valor específico de um csmpo do registro
	-------------------------------------------------------------------------------------- */

		function valor($tab='',$field_value_return='',$value='',$field_check='id'){

			// retorna false caso não encontre

			// $field_value_return 	-> Campo do retorno
			// $field_check 		-> Campo de busca

			$return = array();
			$query = "Select $field_value_return from $tab where $field_check='$value'";		
			
			$mysqli = $this->mysqli;
			$result = $mysqli->query($query);

			if(!$result)
				{
					trigger_error('Erro no BD: '.$query, E_USER_ERROR);
					die();
				}

			if($result)
				{

					if(($result->num_rows)==0)
						{
							return false;
						}

					if(($result->num_rows)>0)
						{
					
							$row = $result->fetch_assoc();
							$valor = $row[$field_value_return];

							// $result->close();

							return $valor;

						}

				}
			}

	/* --------------------------------------------------------------------------------------
	Insert - Retorna o ID Inserido ou false
	-------------------------------------------------------------------------------------- */	
	
		function insert($query=''){

			if($query==''){
				$query = $this -> query_mount();
				}

			$query = $this->query_safe($query);

			$mysqli = $this->mysqli;
			if($result = $mysqli->query($query)) 
				{

					$id = mysqli_insert_id($mysqli);

					// $result->close();

					return $id;

				} else {
					
					$erro = $mysqli->error;
					$this -> error = $erro;

					// $result->close();

					return false;
				}

			}

	/* --------------------------------------------------------------------------------------
	Executa a busca no banco de dados 
	-------------------------------------------------------------------------------------- */

		function query_exec($query=''){

			if($query==''){
				$query = $this -> query_mount();
				}

			$query = $this->query_safe($query);
			
			$mysqli = $this->mysqli;
			if($result = $mysqli->query($query)) 
				{
				
					// $result->close();
					return true;

				} else {
				
					// $result->close();
					return false;

				}

			}

	/* --------------------------------------------------------------------------------------
	Checa se uma tabela existe no banco de dados
	-------------------------------------------------------------------------------------- */

		function check_table_exists($tab){

			$db_name = DB_NAME;

			$sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '$db_name'";

			$db = new db;
			$db->query($sql);
			$result = $db->load();

			$exists = false;
			foreach ($result as $key => $value) 
				{
			
					if($value['TABLE_NAME']==$tab){
						$exists = true;
						}

				}

			// $result->close();
			return $exists;

			}

	/* --------------------------------------------------------------------------------------
	Cria uma tabela de backup
	-------------------------------------------------------------------------------------- */

		function create_backup_table($tab_origin){

			$db = new db;
			$tab_backup = '_backup_'.$tab_origin;
			$tab_exists = $db->check_table_exists($tab_backup);

			if(!$tab_exists)
				{

					/* ---------------------------------
					Criar a tabela de backup
					--------------------------------- */
					$sql_create = "CREATE TABLE $tab_backup SELECT * FROM $tab_origin limit 0";
					$db->query_exec($sql_create);

					$sql_date = "ALTER TABLE  `$tab_backup` ADD  `data_backup` TIMESTAMP NOT NULL AFTER  `data_cadastro`";
					$db->query_exec($sql_date);

					$sql_id = "ALTER TABLE  `$tab_backup` ADD  `id_backup` INT( 9 ) UNSIGNED NOT NULL AFTER  `id`";
					$db->query_exec($sql_id);

					$sql_id = "ALTER TABLE  `$tab_backup` ADD  `id_login_backup` INT( 9 ) UNSIGNED NOT NULL AFTER  `data_backup`";
					$db->query_exec($sql_id);

					$sql_id = "ALTER TABLE  `$tab_backup` ADD  `ip` VARCHAR( 24 ) NOT NULL AFTER  `id_login_backup`";
					$db->query_exec($sql_id);

					$sql_id = "ALTER TABLE  `$tab_backup` ADD UNIQUE (`id`)";
					$db->query_exec($sql_id);

					$sql_id = "ALTER TABLE  `$tab_backup` CHANGE  `id`  `id` INT( 9 ) UNSIGNED NOT NULL AUTO_INCREMENT";
					$db->query_exec($sql_id);

				} 

			}

	/* --------------------------------------------------------------------------------------
	Cria uma tabela de backup
	-------------------------------------------------------------------------------------- */

		function backup($tab,$id){

			$db = new db;
			$tab_backup = '_backup_'.$tab;
			$tab_exists = $db->check_table_exists($tab_backup);

			$user_id = 0;
			$ip = '';

			if(adm_logged())
				{
					$user_id 	= $_COOKIE['userId'];
					$ip 	= $_SERVER['REMOTE_ADDR'];
				}


			/* ---------------------------------
			Verificar se a tabela de backup existe
			--------------------------------- */
			if(!$tab_exists)
				{
					$db->create_backup_table($tab);
				} 
			
			/* ---------------------------------
			Carrega os campos da tabela de origem
			--------------------------------- */
			$campos = $db->load_fields($tab);
			if($campos!=false)
				{

					/* ---------------------------------
					Substitui o campo ID pelo ID_Backup
					--------------------------------- */
					$campos[0] = 'id_backup';
					$campos_sql = implode(',',$campos);
					$sql_insert = 'Insert into '.$tab_backup.' ('.$campos_sql.',id_login_backup,ip)';
				}

			$sql_insert.=' VALUES (';

			/* ---------------------------------
			Buscando os valores dos campos
			--------------------------------- */
			$sql_data = "Select * from $tab where id='$id'";
			$values = $db->load($sql_data);
			
			if(sizeof($values)>0){
				$values_insert = implode("','",$values[0]);
				$sql_insert.="'".$values_insert."','$user_id','$ip')";
				$values = $db->query_exec($sql_insert);
				}

			/* ---------------------------------
			Removendo o backup após a data limite
			--------------------------------- */
			$validate_log_days = DATE_LIMIT_LOG;
			$date_limit_logs = time()-(60*(24*(60*60)));

			$sql_clear = "Delete from $tab_backup where UNIX_TIMESTAMP(data_backup)<'$date_limit_logs'";
			$db->query_exec($sql_clear);

			}
	}
/* --------------------------------------------------------------------------------------------
fim da classe DADOS 
-------------------------------------------------------------------------------------------- */
?>
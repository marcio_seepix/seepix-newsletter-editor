/* ---------------------------------
MODAL
@version Beta v0.4
@date 06.Fev.2018
@author Marcio Camargo
@email marcio@seepix.com.br

Vers�o 0.4 (06/Fev/2018)
- Fechar ao pressionar o ESC;

Vers�o 0.3 (31/Out/2017)
- Reset do conte�do existente em loads anteriores;

Vers�o 0.2 (18/Set/2017)
- Inclu�da a fun��o .destroy();
- Inclu�da a fun��o .width();
- Fechamento automatico ao redimensionar;

--------------------------------- */
/*
var obj_sidebar = new sidebar();
obj_sidebar.start();
obj_sidebar.width(600);
obj_sidebar.url('inc_pag/pag_alvo.php');
obj_sidebar.load();

$("#sidebar-icon").bind('click',function(){
	obj_sidebar.open();
	})
*/

function sidebar(){

	var sidebarWidth 	= 280;
	var sidebarUrlLoad 		= '';
	sidebarTarget = this;

	/* ---------------------------------
	Inclui o c�d HTML e checa o estilo
	--------------------------------- */
	this.start = function()
		{

			/* ---------------------------------
			Incluindo a base em HTML do sidebar
			--------------------------------- */
			var check_cod = $("#sidebar").length;
			if(check_cod==0){
				var cod_html_sidebar = '<div id="sidebar"><div id="sidebar-bg"></div><div id="sidebar-content" class="animated"></div></div>';
				$(cod_html_sidebar).appendTo('#main');
				}

			/* ---------------------------------
			Fechando ao clicar no fundo
			--------------------------------- */
			$(document).on('click',"#sidebar-bg",function(){
				sidebarTarget.close();
				})

			/* ---------------------------------
			Fechando ao redimensionar a tela
			--------------------------------- */
			$(window).resize(function() {
				  sidebarTarget.close();
				});

			/* ---------------------------------
			Fechando o sidebar no ESC
			--------------------------------- */
			document.onkeydown = function(e) {
				    if (e.keyCode == 27) { 
				        sidebarTarget.close();
				    }
				};
		
		}

	this.start();

	/* ---------------------------------
	Define a largura do sidebar
	--------------------------------- */
	this.width = function(value){
		sidebarWidth = value;
		}

	/* ---------------------------------
	Abre o sitebar
	--------------------------------- */
	this.open = function(){

		this.start();
		
		$('#sidebar-content').css('width',sidebarWidth+'px');

		$('#sidebar').show();
		$("#sidebar-bg").show();
		$('#sidebar-content').css('left','0px');
		$('#sidebar-content').removeClass('slideOutLeft').addClass('slideInLeft');
		$("body").addClass('sidebar-open');

		location.hash='menu_open';
		menu_aberto	= 1;

	}

	/* ---------------------------------
	Fecha o sidebar
	--------------------------------- */
	this.close = function(){

		$('#sidebar-content').removeClass('slideInLeft').addClass('slideOutLeft');
		$("#sidebar-bg").hide();
		$("body").removeClass('sidebar-open');
		
		setTimeout(function(){
			$("#sidebar").hide();
			}, 1000);

		}

	/* ---------------------------------
	Carrega um conte�do externo
	--------------------------------- */
	this.url = function(endereco){
		sidebarUrlLoad = endereco;
		}

	/* ---------------------------------
	Define um conte�do para o sidebar
	--------------------------------- */
	this.load = function(){

			$("#sidebar-content").html('');

			$.ajax(
				{
				  	url: sidebarUrlLoad,
				  	cache: false,
				  	contentType: "application/x-www-form-urlencoded;",
				  	beforeSend: function(){},
				  	success: function(html){
							$("#sidebar-content").html(html);
						}
					
				});
		}

	/* ---------------------------------
	Define um tempo para fechar o sidebar
	--------------------------------- */
	this.timeout = function(value){

		setTimeout(function(){
			sidebarTarget.close();
			},value);

		}

	/* ---------------------------------
	Remove completamente o cod html e estilo
	--------------------------------- */
	this.destroy = function(){
		$("#sidebar-content").html('');
		}

	}
/* ---------------------------------
MODAL
@version Beta v0.1
@date 20.Jun.2016
@author Marcio Camargo
@email marcio@marciocamargo.com.br

--------------------------------- */

	/*
		--- COMO UTILIZAR ---
		
		objeto = new message();
		objeto.classPosition('t-c');
		objeto.add('String');				

		-- opcionais --
		objeto.duration('Number');
	
	*/

function message(){

	targetMsg 		= this;
	duration 		= 3.0;
	classPosition 	= "t-c";	
	classAnimateIn 	= "slideInUp";
	classAnimateOut = "flipOutX";

	/* ---------------------------------
	Define o tempo de exibi��o da mensagem
	--------------------------------- */
	this.duration = function(valor){
		duration = valor;
		}

	/* ---------------------------------
	Define a posi��o da mensagem na tela
	-> b-l  Bottom/Left
	-> b-r	Bottom/Right
	-> b-c	Bottom/Center
	-> t-r	Top/Right
	-> t-l	Top/Left
	-> t-c	Top/Center
	--------------------------------- */
	this.classPosition = function(valor){
		classPosition = valor;
		}

	/* ---------------------------------
	Inicia a base da mensagem
	--------------------------------- */
	this.start = function()
		{

			var cod_html_sidebar = '<div id="message" class="'+classPosition+'"><div id="message-stage"></div></div>';
			$(cod_html_sidebar).appendTo('#main');

		}

	/* ---------------------------------
	Adiciona uma mensagem
	--------------------------------- */
	this.add = function(text)
		{

			if(classPosition=='t-r' || classPosition=='t-l' || classPosition=='t-c'){
				classAnimateIn = "slideInDown";
				}

			var messageCount = $("#message").length;
			if(messageCount==0){
				targetMsg.start();
				}

			var zIndex = 600;
			$("#message").css("zIndex", zIndex);

			var token = createToken();
			var cod = '<div id="'+token+'" class="message_item_component"><div class="rounded shadow font-2 font-size-3 animated '+classAnimateIn+'" style="z-index:'+zIndex+'">'+text+'</div></div>';
			$(cod).appendTo('#message-stage');

			durationTime = duration * 1000;
			setTimeout(function() {
				$("#"+token).children('div').removeClass(classAnimateIn).addClass(classAnimateOut);
				}, durationTime);

			setTimeout(function() {
				$("#"+token).animate({ 
				        height: "0px"
				      }, 1000);
				}, durationTime+1000);

			setTimeout(function() {
				$("#"+token).remove();

				/* ---------------------------------
				Checando se existe mais alguma 
				mensagem em andamento para a
				remo��o da base
				--------------------------------- */
				var restantes = $('#message-stage').children('div').length;
				
				if(restantes==0)
					{
						$("#message").remove();
					}

				}, durationTime+2000);

		}

}
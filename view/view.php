<?php
/* ---------------------------------
@version Beta v0.1
@date 23.aBR.2018
@author Marcio Camargo
@email marcio@seepix.com.br

Esta página disponibiliza 
as views de conteúdo
em saída no formato json.

--------------------------------- */

include_once('../config/config.php');
include_once('../php/class/db.class.php');
include_once('../php/functions.php');

$conteudo = array();
$conteudo['status'] 	= 0;
$conteudo['message'] 	= '';
$conteudo['data'] 		= '';

$db = new db;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MONTANDO A SQL DA VIEW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$sql = "Select 
			v.id,
			v.cod,
			v.id_tipo as tipo_id,
			t.tipo as tipo,
			v.titulo,
			v.description,
			v.ativo
		from 
			view as v
			left join page_tipo as t on (v.id_tipo = t.id)
		where 
			v.ativo='1' ";

/* ---------------------------------
Buscando uma view por id
--------------------------------- */
if(isset($_GET['id']))
	{
		$id = (integer)$_GET['id'];
		$sql.=" and v.id='$id' ";
	}

/* ---------------------------------
Buscando uma view por código
--------------------------------- */
if(isset($_GET['cod']))
	{
		$cod = get($_GET['cod']);
		$sql.=" and v.cod='$cod'";
	}

$sql.=" order by v.titulo asc ";

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FIM DA MONTAGEM DA SQL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$dados = $db -> load($sql);
if(sizeof($dados)==0)
	{
		$conteudo['status'] 	= '-1';
		$conteudo['message'] 	= 'Esta view não está ativa ou foi removida';
	}

if(sizeof($dados)>0)
	{

		foreach ($dados as $key => $value) 
			{
				
				$id_view = $value['id'];

				$dados[$key]['file'] = 'view/'.$value['cod'].'/view.html';
				$dados[$key]['thumbs'] = 'view/'.$value['cod'].'/thumb.png';

				$sql_fields = "Select id,cod,label,type from view_fields where id_view='$id_view' order by ordem asc";
				$dados_fields = $db -> load($sql_fields);

				$dados[$key]['fields'] = $dados_fields;

			}		

		$dados = json_encode_utf8($dados);
		$conteudo['status'] = 1;
		$conteudo['data'] 	= $dados;
	}


header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($conteudo);
?>
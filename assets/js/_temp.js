$(function(){
	view_load();
})

function view_load()
	{
		fetch('view/dois_textos/view.html',{ 
				method: 'GET',
				mode: 'no-cors',
				credentials: "same-origin"
			})
			.then(function(response){
				return response.text();
			})
			.then(function(data){
				console.log(data);
				$(data).appendTo('#content-temp');
			})
			.then(function(){
				
				return fetch('teste/view.php',{ 
						method: 'GET',
						mode: 'no-cors',
						credentials: "same-origin"
					})
					.then(function(response){
						return response.json();
					})
					.catch(function(err){
						console.log(err);
					})				

			}).then(function(data_json){

				$.each(data_json.dados.content, function( key, value )
					{
						var field_cod = value.cod;
						var field_value = value.value;

						$("[data-view_field="+field_cod+"]").html(field_value);

				  		console.log('Dados: ',value);
					});

				console.log('data_json',data_json);
			})
			.catch(function(err){
				console.log(err);
			})
	}

$(function(){
	view_load();	
})

function view_load()
	{
		fetch('teste/view.php',{ 
				method: 'GET',
				mode: 'no-cors',
				credentials: "same-origin"
			})
			.then(function(response){
				return response.json();
			})
			.then(function(data){

				var html_file = data.data.view.file;
				var conteudo = data.data.content;

				fetch(html_file,{ 
						method: 'GET',
						mode: 'no-cors',
						credentials: "same-origin"
					})
					.then(function(response){
						return response.text();
					})
					.then(function(data){
						$(data).appendTo('#content-temp');
					})
					.then(function(){
						
						$.each(conteudo, function( key,value)
							{
								var field_cod = value.cod;
								var field_value = value.value;
								var field_type = value.type;

								if(field_type=='text'){
										$("[data-view_field="+field_cod+"]").html(field_value);
									}

								if(field_type=='background-image'){
										$("[data-view_field="+field_cod+"]").css('background-image','url('+field_value+')');
									}

							});

					})
					.catch(function(err){
						console.log(err);
					})
				
			})
			.catch(function(err){
				console.log(err);
			})
	}

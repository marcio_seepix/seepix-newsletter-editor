function ajax(){

	var url 			= '';
	var func_pre_send 	= '';
	var func_pos_send 	= '';
	var time_limit		= 60;
	var retorno			= false;

	this.setUrl = function(value){
		url = value; 			
		};

	this.func_pre_send = function(value){
		func_pre_send = value;
		};

	this.func_pos_send = function(value){
		func_pos_send = value;
		};

	this.exec = function(){

			if(url==''){
				return false;
				}

			$.ajax(
					{
					  	url: url,
					  	cache: false,
					  	type : 'POST',
					  	contentType: "application/x-www-form-urlencoded;",
					  	beforeSend: function()
					  		{

						  		/* ---------------------------------
								Executando uma fun��o antes do envio
								--------------------------------- */
								if(func_pre_send!='')
									{
									
										exec = func_pre_send;
										exec();			
									
									}

					  		},
					  	success: function(html){

					  			/* ---------------------------------
								Executando uma fun��o antes do envio
								--------------------------------- */
								if(func_pos_send!='')
									{
									
										exec = func_pos_send;
										exec(html);			
									
									} 
							
							}
						
					});

		};
	}
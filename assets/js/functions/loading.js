function loading(){

	var time_limit = 30; /* em segundos */	
	var cod='<div id="loading" class="pos-t-l full-size pos-full" style="z-index:100; position:fixed"><div class="pos-t-l full-size pos-full bg-grey-dark opacity-90"></div><div id="loading-icon" class="pos-middle animated fadeIn"><i class="loading spinner icon big white"></i></div></div>';

	var confere = $('#loading').length;
	if(confere==0){
		$(cod).prependTo('#main');
		}
	
	/* ---------------------------------
	Removendo automaticamente no limite
	--------------------------------- */

	time_limit = time_limit * 1000;
	setTimeout(function() {
		var confere = $('#loading').length;
		if(confere==1){
			
			$("#loading-icon").children('i').removeClass('loading').removeClass('spinner').addClass('attention').addClass('red');

			setTimeout(function() {
				$("#loading-icon").removeClass('slideInUp').addClass('flipOutX');
				}, 1000);

			setTimeout(function() {
				$('#loading').remove();
				}, 2000);

			}

		}, time_limit);

	}
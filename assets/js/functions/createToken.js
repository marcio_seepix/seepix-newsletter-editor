function createToken(size){
	
	if(typeof size=='undefined'){
							size = 20;
							}
											
	var chars = "abcdefghijkmnopqrstuvxyz23456789";
	var limite = chars.length;
	var token='';
	
	for(var i=0;i<size;i++){
			var pos=Math.floor(Math.random()*(limite-1));
			token=token+chars.charAt(pos);
			}
			
	return token;
	}
function form_contar_caracteres(id_campo,limite,id_contador){
	
	//onKeyUp="form_contar_caracteres(this.id,250,'id_contador')"
	
	var campo = $("#"+id_campo);
	var tam_texto;
	var texto_cortado;
	var campo_contagem = $("#"+id_contador);
	var restante;
	var texto;
	
	tam_texto = $("#"+id_campo).val().length;

	if(tam_texto>limite){
		texto = $("#"+id_campo).val();
		texto_cortado = texto.substring(0,limite);
		$("#"+id_campo).val(texto_cortado);
		} else {
			restante = limite-tam_texto;
			campo_contagem.text(restante);
			}
	}
function replaceStr(texto,caractere_original,novo_caractere){
	
	if(texto=='' || typeof(texto)=='undefined'){
		return texto;
		}
	
	var texto = String(texto);
	
	while(texto.indexOf(caractere_original)!= -1) {
 		texto = texto.replace(caractere_original,novo_caractere);
		}
	return texto;
	}
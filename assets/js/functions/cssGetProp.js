function cssGetProp(id,prop){
	
	var elem = document.getElementById(id);
	if(elem==null){ return false; }

	var styleDom = document.defaultView.getComputedStyle(elem, null);
	var value = styleDom.getPropertyValue(prop);

	if(value==''){
		return false;
		}

	return value;

	}
function cssGetFullProp(id){
	
	var elem = document.getElementById(id);
	if(elem==null){ return false; }

	var style = document.defaultView.getComputedStyle(elem, null);
	return style;

	}
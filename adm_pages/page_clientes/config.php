<?php
/* ---------------------------------
@version Beta v0.1
@date 12.Jul.2016
@author Marcio Camargo
@email marcio@marciocamargo.com.br

Gerencia os clientes participantes
do concurso
--------------------------------- */

$config['page']['standard'] 				= 1;
$config['page']['output_json'] 				= 0;

/* ---------------------------------
Configura��o de p�gina padr�o
--------------------------------- */
$config['tab'] 								= 'page_clientes';
$config['title'] 							= 'Clientes';
$config['description'] 						= 'Selecione um cliente';
$config['unit_per_pag']						= 30;
$config['order']['field']					= 'id';
$config['order']['mode']					= 'desc';

/* ---------------------------------
Configura��es do menu
--------------------------------- */
$config['menu']['status'] 					= 1;
$config['menu']['label'] 					= 'Clientes';
$config['menu']['section'] 					= 'page_clientes';
$config['menu']['subsection'] 				= '';
$config['menu']['icon'] 					= 'chevron right';
$config['menu']['adm_dir'] 					= 'page_clientes';

/* ---------------------------------
Funcionalidades
--------------------------------- */
$config['new']['status'] 					= 1;
$config['new']['label'] 					= 'Adicionar';
$config['delete']['status'] 				= 1;
$config['active']['status'] 				= 1;

/* ---------------------------------
Campos
--------------------------------- */
$config['field'][0]['db_field'] 			= 'cliente';		/* Nome do campo na tabela do BD */
$config['field'][0]['required'] 			= 1;				/* Campo obrigat�rio */
$config['field'][0]['on_list'] 				= 1;				/* Inclu�do no formul�rio de novo registro */

$config['field'][1]['type']					= 'text';
$config['field'][1]['db_field'] 			= 'obs';			/* Nome do campo na tabela do BD */
$config['field'][1]['required'] 			= 0;				/* Campo obrigat�rio */
$config['field'][1]['on_list'] 				= 0;				/* Inclu�do no formul�rio de novo registro */

?>
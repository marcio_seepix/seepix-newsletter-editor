<?php
/* ---------------------------------
@version Beta v0.1
@date 12.Jul.2016
@author Marcio Camargo
@email marcio@marciocamargo.com.br

Gerencia os clientes participantes
do concurso
--------------------------------- */

$config['page']['standard'] 				= 1;
$config['page']['output_json'] 				= 0;

/* ---------------------------------
Configura��o de p�gina padr�o
--------------------------------- */
$config['tab'] 								= 'page_tipo';
$config['title'] 							= 'Tipos de conte�dos';
$config['description'] 						= 'Gerencie os tipos de conte�dos edit�veis: Newsletter, Landing Page, etc...';
$config['unit_per_pag']						= 30;
$config['order']['field']					= 'id';
$config['order']['mode']					= 'desc';

/* ---------------------------------
Configura��es do menu
--------------------------------- */
$config['menu']['status'] 					= 1;
$config['menu']['label'] 					= 'Tipos de conte�dos';
$config['menu']['section'] 					= 'page_tipo';
$config['menu']['subsection'] 				= '';
$config['menu']['icon'] 					= 'chevron right';
$config['menu']['adm_dir'] 					= 'page_tipo';

/* ---------------------------------
Funcionalidades
--------------------------------- */
$config['new']['status'] 					= 1;
$config['new']['label'] 					= 'Adicionar';
$config['delete']['status'] 				= 1;
$config['active']['status'] 				= 1;

/* ---------------------------------
Campos
--------------------------------- */
$config['field'][0]['db_field'] 			= 'tipo';			/* Nome do campo na tabela do BD */
$config['field'][0]['required'] 			= 1;				/* Campo obrigat�rio */
$config['field'][0]['on_list'] 				= 1;				/* Inclu�do no formul�rio de novo registro */
		
?>
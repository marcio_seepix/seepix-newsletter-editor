<div id="dv-page-editor" data-version="<?php print $news_version; ?>">
    <div id="dv-page-editor-overlay"></div>

    <!-- STAGE PRINCIPAL DE EDI��O !-->
    <div id="dv-page-editor-main" class="bg-white">
        <div id="dv-page-editor-main-title"><p class="font-size-6">Construtor de p�ginas</p></div>
        <div id="dv-page-editor-main-config">
            <div id="btn-page-editor-edit-views" class="ui icon button basic labeled small">
                <i class="setting icon"></i>
                Views
            </div>
        </div>
        <div id="adm-pages-stage"></div>
    </div>

    <!-- FORMUL�RIO PARA A EDI��O DE CONTE�DO DA VIEW !-->
    <div id="dv-page-editor-content-form" class="hide" data-id_view="">
        <div id="dv-page-editor-content-form-background" class="pos-full"></div>
        <div id="dv-page-editor-content-form-stage" class="animated slideInLeft">
            <div class="pad-20">
                <p class="bold black font-size-5">Editar Conte�do</p>
                <p class="font-size-3">Edite o conte�do de sua p�gina</p>

                <div class="ui top attached tabular menu">
                    <a class="item active" data-tab="news-content-editor-conteudo">Conte�do</a>
                </div>
                <div class="ui bottom attached tab segment active" data-tab="news-content-editor-conteudo">
                    <div id="dv-page-editor-content-form-stage-fields" class="ui form margin-t-20">
                        <i class="icon loading spinner"></i>
                    </div>
                </div>
                <div class="text-r margin-t-20">
                    <div id="btn-page-editor-content-form-cancel" class="ui labeled icon button tiny">
                        <i class="reply icon"></i>
                        Cancelar
                    </div>
                    <div id="btn-page-editor-content-form-confirm" class="ui labeled icon button  tiny green">
                        <i class="save icon"></i>
                        Salvar
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL PARA ADICIONAR UMA NOVA P�GINA !-->
    <div class="ui modal" id="dv-page-editor-new-form">
        <i class="close icon"></i>
        <div class="header">
            Adicionar p�gina
        </div>
        <div class="content">
            <div class="description">
                <div class="bg-grey-ultralight margin-b-20 pad-20 rounded">
                    <div class="ui form">
                        <div class="field">
                            <label>T�tulo da sua nova p�gina</label>
                            <input id="cp-page-editor-new-title" type="text">
                        </div>
                        <div class="fields">
                            <div class="field wide four">
                                <label>Cliente</label>
                                <select id="cp-page-editor-new-client"></select>
                            </div>
                            <div class="field wide four">
                                <label>Tipo</label>
                                <select id="cp-page-editor-new-tipo-page"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-page-editor-form-new-error"></div>
        </div>
        <div class="actions">
            <div id="btn-page-editor-new-cancel" class="ui labeled icon button">
                <i class="reply icon"></i>
                Cancelar
            </div>
            <div id="btn-page-editor-new-save" class="ui labeled icon button green">
                <i class="save icon"></i>
                Criar p�gina
            </div>
        </div>
    </div>

    <!-- MODAL PARA CONFIRMA��O DE REMO��O DE VIEW !-->
    <div class="ui modal" id="dv-page-editor-view-confirm-remove" data-id="" data-id_page="" data-id_view="">
        <div class="header">
            <i class="trash icon"></i> Remover conte�do
        </div>
        <div class="content">
            <div class="description">
                <p>Tem certeza de que deseja remover definitivamente este conte�do?</p>
            </div>
            <div class="modal-page-editor-form-new-error"></div>
        </div>
        <div class="actions">
            <div id="btn-page-editor-view-confirm-remove-cancel" class="ui labeled icon button">
                <i class="reply icon"></i>
                Cancelar
            </div>
            <div id="btn-page-editor-view-confirm-remove-confirm" class="ui labeled icon button red">
                <i class="trash icon"></i>
                Excluir
            </div>
        </div>
    </div>

    <!-- MODAL PARA CONFIRMA��O DE REMO��O DE P�GINA !-->
    <div class="ui modal" id="dv-page-editor-page-remove" data-id="">
        <div class="header">
            <i class="trash icon"></i> Remover p�gina
        </div>
        <div class="content">
            <div class="description">
                <p class="bold margin-0">Tem certeza de que deseja remover esta p�gina definitivamente?</p>
            </div>
        </div>
        <div class="actions">
            <div id="btn-page-editor-page-remove-cancel" class="ui labeled icon button">
                <i class="reply icon"></i>
                Cancelar
            </div>
            <div id="btn-page-editor-page-remove-confirm" class="ui labeled icon button red">
                <i class="trash icon"></i>
                Excluir
            </div>
        </div>
    </div>


    <!-- MODAL PARA CONFIRMA��O DE REMO��O DE M�DIA !-->
    <div class="ui modal" id="dv-page-editor-uploads-confirm-remove" data-id="">
        <div class="header">
            <i class="trash icon"></i> Remover m�dia
        </div>
        <div class="content">
            <div class="description">
                <p class="bold margin-0">Tem certeza de que deseja remover da biblioteca?</p>
                <p class="margin-0">Ao excluir um arquivo, este ser� removido da biblioteca, por�m continuar� ativo nos conte�dos onde foi utilizado.</p>
            </div>
            <div class="modal-page-editor-form-new-error"></div>
        </div>
        <div class="actions">
            <div id="btn-page-editor-uploads-confirm-remove-cancel" class="ui labeled icon button">
                <i class="reply icon"></i>
                Cancelar
            </div>
            <div id="btn-page-editor-uploads-confirm-remove-confirm" class="ui labeled icon button red">
                <i class="trash icon"></i>
                Excluir
            </div>
        </div>
    </div>
</div>
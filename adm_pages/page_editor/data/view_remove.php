<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Conte�do removido com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a inser��o da p�gina.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Erro no recebimento de informa��es da p�gina.';
$msg_retorno[-3] = 'Este item n�o p�de ser removido.';
$msg_retorno[-4] = 'Ocorreu um erro durante a remo��o deste conte�do.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_GET['id_item']) && isset($_GET['id_view']) && isset($_GET['id_page']))
			{
				$id_item = (integer)$_GET['id_item'];
				$id_page = (integer)$_GET['id_page'];
				$id_view = (integer)$_GET['id_view'];

				if($id_item>0)
					{
						
						// REMOVER O CONTE�DO DA P�GINA	
						$db = new db;
						$sql_delete = "Delete from page_views where id='$id_item' and id_page='$id_page' and id_view='$id_view'";
						$res = $db->query_exec($sql_delete);

						if($res)
							{
								$status = 1;
							} else {
								$status = -4;
							}

					} else {
						$status = -3;		
					}

			} else {
				$status = -2;
			}
		
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
$content['data'] 	= '';

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

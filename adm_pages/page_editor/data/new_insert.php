<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$response_msg['-1'] = 'Voc� n�o est� conectado no momento';

$msg_retorno[1] = 'P�gina inserida com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a inser��o da p�gina.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Erro no recebimento do t�tulo da p�gina.';
$msg_retorno[-3] = 'Erro no recebimento do cliente da p�gina.';
$msg_retorno[-4] = 'Erro no recebimento do tipo de p�gina.';
$msg_retorno[-5] = 'O t�tulo da p�gina n�o foi preenchido.';
$msg_retorno[-6] = 'O Cliente da p�gina n�o foi informado.';
$msg_retorno[-7] = 'O tipo da p�gina n�o foi informado.';
$msg_retorno[-8] = 'J� existe uma p�gina com este t�tulo, deste cliente.';
$msg_retorno[-9] = 'Ocorreu um erro durante a inser��o da p�gina.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		/* ---------------------------------
		Recebendo as vari�veis
		--------------------------------- */
		if(!isset($_POST['title']))		{	$status = -2;		}
		if(!isset($_POST['cliente']))	{	$status = -3;		}
		if(!isset($_POST['tipo']))		{	$status = -4;		}

		/* ---------------------------------
		Checando as vari�veis
		--------------------------------- */
		if($status==0)
			{
					
				$title 		= get($_POST['title']);
				$cliente 	= (integer)$_POST['cliente'];
				$tipo 		= (integer)$_POST['tipo'];

				$title = encode_iso($title);		

				/* ---------------------------------
				Erros de tipo/preenchimento
				--------------------------------- */
				if($title==""){		$status = -5;	}
				if($cliente<1){		$status = -6;	}
				if($tipo<1){		$status = -7;	}

			}

		/* ---------------------------------
		Checando se a p�gina j� existe 
		com a mesma tag
		--------------------------------- */
		if($status==0)
			{	

				$db = new db;

				$tag = tag($title);
				$contar = $db->num_rows("Select * from page where tag='$tag' and id_tipo='$tipo' and id_cliente='$cliente'");

				if($contar>0)
					{
						$status = -8;	
					}

			}

		/* ---------------------------------
		Inserindo a p�gina
		--------------------------------- */
		if($status==0)
			{
				
				$sql_insert = "Insert into page (title,tag,id_tipo,id_cliente,data,ativo) VALUES ('$title','$tag','$tipo','$cliente',now(),'1')";
				$check_insert = $db->insert($sql_insert);

				if($check_insert)
					{
						$content['data'][0]['id'] = $check_insert;
						$status = 1;
					}

				if(!$check_insert)
					{
						$status = -9;
					}

			}
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

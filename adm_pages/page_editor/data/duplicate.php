<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'C�pia efetuada com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro desconhecido durante o processo de c�pia.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Erro no recebimento de dados.';
$msg_retorno[-3] = 'Esta p�gina n�o est� mais dispon�vel.';
$msg_retorno[-4] = 'Ocorreu um erro durante a inser��o no Banco de Dados.';
$msg_retorno[-5] = 'Ocorreu um erro durante o processo.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Recebendo os dados
--------------------------------- */
if($status==0)
	{
		if(!isset($_GET['page_id']))	
			{
				$status = -2;			
			}
	}

/* ---------------------------------
Carregando os dados da p�gina principal
--------------------------------- */
if($status==0)
	{

		$page_id = (integer)$_GET['page_id'];
	
		$db = new db;
		$page = $db -> load("Select 
									id_cliente,
									id_tipo,
									id_section,
									CONCAT(title, ' (duplicado)') as title,
									tag,
									content,
									width,
									language 
								from 
									page 
								where id='$page_id'");

		if(sizeof($page)==0)
			{
				$status = -3;	
			}
	}

/* ---------------------------------
Inserindo a nova p�gina
--------------------------------- */	
if($status==0)
	{
		$id_new_page = $db ->insert_obj($page[0],'page');
		if($id_new_page===false)
			{
				$status = -4;
			}
	}


if($status==0)
	{

		/* ---------------------------------
		Esse array utilizamos para atualizar
		os ids da inser��o da view para
		os novos IDs que vamos inserindo
		na tabela PAGE_VIEWS
		--------------------------------- */
		$views_id_content_changed = array();

		$views = $db -> load("Select id, $id_new_page as id_page, id_view, ordem from page_views where id_page='$page_id'");
		foreach ($views as $key => $value)
			{
				
				$id_view_content_id = $value['id'];
				
				/* ---------------------------------
				Removendo o ID pois n�o � utilizado
				na inser��o no mysql
				--------------------------------- */
				unset($value['id']);
				$id_new_view_id = $db ->insert_obj($value,'page_views');
				$views_id_content_changed[$id_view_content_id] = $id_new_view_id;

			}

		$page_content = $db -> load("Select 
										$id_new_page as page_id,
										content_view_id,
										view_id,
										view_field_cod,
										view_field_type,
										value 
									from 
										page_content 
									where 
										page_id='$page_id'");

		foreach ($page_content as $key => $value) 
			{
				$value['content_view_id'] = $views_id_content_changed[$value['content_view_id']];
				$id_new_view_id = $db ->insert_obj($value,'page_content');
			}

		$status = 1;

	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);


header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

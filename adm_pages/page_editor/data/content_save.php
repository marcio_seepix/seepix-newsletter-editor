<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados salvos com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualiza��o de conte�do.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informa��es.';
$msg_retorno[-3] = 'Ocorreu um erro durante a inser��o no Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_POST['config']))
			{

				$config = $_POST['config'];
				if(gettype($config)=='array')
					{

						$db = new db;

						foreach ($config as $key => $value)
							{
							
								if($status==0)
									{

										$page_id = $value['page_id'];
										$view_id = $value['view_id'];
										$view_field_cod = $value['view_field_cod'];
										$view_field_type = $value['view_field_type'];
										$content_view_id = $value['content_view_id'];
										$conteudo = $value['value'];

										/* ---------------------------------
										Contando se o item existe
										--------------------------------- */
										$sql_count = "Select * from page_content where content_view_id='$content_view_id' and view_field_cod='$view_field_cod'";
										$contar = $db->num_rows($sql_count);
										
										/*---------------------------------
										Atualizando o item
										--------------------------------- */
										if($contar>0)
											{
												$sql = "Update page_content set value='$conteudo', view_field_type='$view_field_type' where content_view_id='$content_view_id' and view_field_cod='$view_field_cod'";
											} else {
												$sql = "Insert into page_content (
																				page_id,
																				content_view_id,
																				view_id,
																				view_field_cod,
																				view_field_type,
																				value,
																				data
																			) VALUES (
																				'$page_id',
																				'$content_view_id',
																				'$view_id',
																				'$view_field_cod',
																				'$view_field_type',
																				'$conteudo',
																				now()
																			)";
											}

										$result = $db->query_exec($sql);
										if(!$result)
											{
												$status = -3;
											}
									}
							}

						if($status==0)
							{
								$status = 1;
							}
					}

			} else {
				$status = -2;
			}

	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

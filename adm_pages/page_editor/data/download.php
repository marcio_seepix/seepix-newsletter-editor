<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Arquivo ZIP gerado com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante o empacotamento do conteúdo.';
$msg_retorno[-1] = 'Você não está conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informações.';
$msg_retorno[-3] = 'Ocorreu um erro durante a gravação do arquivo no diretório.';

/* ---------------------------------
Usuário desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usuário logado
--------------------------------- */
if($status==0)
	{
		if(!isset($_POST['cod']))
			{
				$status = -2;	
			}
	}

if($status==0)
	{	

		/* ---------------------------------
		Diret�rio de imagens
		--------------------------------- */
		$url_local_img = $serverProtocol.$serverUrl.'/fotos/midias/';

		/* ---------------------------------
		Recebendo o código HTML
		--------------------------------- */
		$cod_html = $_POST['cod'];
		$url_img = $_POST['url_img'];

		if($url_img=='')
			{
				$url_img = '(URL)';
			} else {

				/* ---------------------------------
				Removendo a barra no final da URL
				--------------------------------- */
				if(substr($url_img,-1)=='/')
					{
						$url_img = substr($url_img,0,-1);
					}
			}

		/* ---------------------------------
		Lendo o DOM do código HTML
		--------------------------------- */
	    $dom_cod = new DOMDocument();
	    $dom_cod->loadHTML($cod_html);

	    /* ---------------------------------
	    Coletando a lista de imagens
	    --------------------------------- */
	    $imageTags = $dom_cod->getElementsByTagName('img');

	    /* ---------------------------------
	    Substituindo a url de imagens
	    --------------------------------- */
		$cod_html = str_replace($url_local_img,$url_img."/", $cod_html);

		/* ---------------------------------
		Iniciando o ZIP file
		--------------------------------- */
		$zip = new ZipArchive();

		$zip_filename = "news_".time().".zip";
		$zip_final_url = $serverProtocol.$serverUrl.'/fotos/temp/'.$zip_filename;
		$filename = "../../../fotos/temp/".$zip_filename;
		if($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
		    exit("cannot open <$filename>\n");
			}

		/* ---------------------------------
		Ajustando o cabeçalho do código HTML
		--------------------------------- */
		$cod_html = '<html><title></title><body>'.$cod_html.'</body></html>';
		$zip->addFromString("index.html", $cod_html);

		/* ---------------------------------
		Adicionando as imagens no ZIP
		--------------------------------- */
		foreach($imageTags as $tag)
			{
		        $img_temp = $tag->getAttribute('src');
		        $img_temp_name = str_replace($url_local_img,"", $img_temp);
		        $zip->addFile('../../../fotos/midias/'.$img_temp_name,$img_temp_name);
	    	}

		$zip->close();
	    $file_name = basename($filename);

	    if(file_exists($filename))
	    	{
	    		$response['data'] = $zip_final_url;
	    		$status = 1;
	    	} else {
	    		$status = -3;
	    	}

	}

$response['status'] 	= $status;
$response['message'] = utf8_encode($msg_retorno[$status]);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

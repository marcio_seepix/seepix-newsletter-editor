<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados salvos com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualiza��o de conte�do.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informa��es.';
$msg_retorno[-3] = 'Ocorreu um erro durante a inser��o no Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		$fields = array(
				'id_page',
				'status',
				'titulo',
				'width',
				'margin_bottom',
				'secao',
				'content',
				'url_img_final');

		if(isset($_POST['data']))
			{

				$data = $_POST['data'];
				foreach ($fields as $key => $value)
					{
						$$value = encode_iso($data[$value]);
					}

				$sql_update = "Update page set 
										ativo = '$status',
										title = '$titulo',
										width = '$width',
										margin_bottom = '$margin_bottom',
										id_section = '$secao',
										content = '$content',
										url_img_final = '$url_img_final'
									where 
										id = '$id_page'";

				$db = new db;
				$result = $db->query_exec($sql_update);
				if($result)
					{
						$status = 1;	
					} else {
						$status = -3;	
					}

			} else {
				$status = -2;
			}
	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

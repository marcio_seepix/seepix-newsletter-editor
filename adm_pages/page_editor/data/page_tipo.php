<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Tipos de p�ginas selecionados com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a sele��o dos tipos de p�gina.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro durante a conex�o com o Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if($status==0)
			{
				$sql_command = "Select id,tipo from page_tipo where ativo='1'";

				$db = new db;
				
				$result = $db->load($sql_command);

				if(sizeof($result) > 0)
					{

						$content['data'] = $result;
						$status = 1;

					} else {
						$status = -2;
					}
			}
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
$content['data'] 	= json_encode_utf8($content['data']);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

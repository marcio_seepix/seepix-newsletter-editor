<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'P�gina removida com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a remo��o da p�gina.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informa��es.';
$msg_retorno[-3] = 'Ocorreu um erro durante a remo��o do conte�do.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_GET['id_del']))
			{

				$id_del = (integer)$_GET['id_del'];
				$db = new db;
				$db -> backup('page',$id_del);

				$sql_del = "Delete from page where id='$id_del'";
				$result = $db->query_exec($sql_del);
				if($result)
					{
						$status = 1;	
					} else {
						$status = -3;	
					}

			} else {
				$status = -2;
			}

	}

$response['status'] 	= $status;
$response['message'] = utf8_encode($msg_retorno[$status]);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

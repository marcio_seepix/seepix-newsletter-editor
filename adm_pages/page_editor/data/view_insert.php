<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'View criada com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a criação da view.';
$msg_retorno[-1] = 'Você não está conectado.';
$msg_retorno[-2] = 'Erro no recebimento de informações.';
$msg_retorno[-3] = 'Esta view não está mais disponível ou foi removida.';
$msg_retorno[-4] = 'Erro na criação do diretório da view.';
$msg_retorno[-5] = 'Ocorreu um erro durante a inserção no Banco de Dados.';

/* ---------------------------------
Usuário desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Recebimento de dados
--------------------------------- */
if($status==0)
	{
		if(!isset($_POST['data']))
			{
				$status = -2;
			}		
	}

/* ---------------------------------
Usuário logado
--------------------------------- */
if($status==0)
	{

		$db = new db;

		$data = $_POST['data'];
		$id = $data['id'];
		$titulo = $data['titulo'];
		$description = $data['description'];
		$cod_html = $data['cod_html'];
		$img = $data['img'];
		$fields = $data['fields'];

		$id = 0;
		if($id>0)
			{
				/* ---------------------------------
				Verificando se a view está disponível
				--------------------------------- */
				$view_dados = $db -> load("Select * from view where id='$id'");
				if(sizeof($view_dados)==0)
					{
						$status = -3;
					}

				if(sizeof($view_dados)>0)
					{
						$directory_view = $view_dados[0]['cod'];
					}

			} else {
				
				/* ---------------------------------
				Checando se o diretório da view existe,
				se não existe, tenta criar
				--------------------------------- */
				$cod_view = tag($titulo);
				$dir_created = false;
				$i = 0;
				while($dir_created==false)
					{
						
						$cod_view_check = $cod_view;

						print 'Dir: '.$cod_view_check.'<br>';

						if(is_dir('../../../view/'.$cod_view_check.'/'))
							{
								$cod_view_check = $cod_view.'_'.$i;
							} else {
								mkdir('../../../view/'.$cod_view_check.'/');
								$dir_created = true;
							}

						if($i>20)
							{
								$status = -4;
							}

						$i++;
					}
			}
	}

if($status==0)
	{

		echo '<pre>';
			print_r($view_dados);
		echo '</pre>';

		

			
	}

if(adm_logged())
	{

	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
$content['data'] 	= json_encode_utf8($content['data']);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

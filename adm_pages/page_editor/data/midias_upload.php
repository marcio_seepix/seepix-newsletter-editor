<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Arquivo enviado com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante o envio do arquivo.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro durante o recebimento de conte�do.';
$msg_retorno[-3] = 'O diret�rio de imagens n�o existe.';
$msg_retorno[-4] = 'Ocorreu um erro durante a c�pia do arquivo para o diret�rio final';
$msg_retorno[-5] = 'Ocorreu um erro durante a inser��o no BD';

$dir_upload = '../../../adm/components/upload_html5/upload_files/';
$dir_images = '../../../fotos/midias/';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_COOKIE['userId'])){
			$id_user = $_COOKIE['userId'];
			}

		if(!isset($_POST['cod']) || !isset($_POST['nome']) || !isset($_POST['mime']))
			{
				$status = -2;				
			}

		/* ---------------------------------
		Verificando se o diretorio de midias 
		n�o existe e tenta cri�-lo
		--------------------------------- */
		if(!is_dir($dir_images))
			{
				mkdir($dir_images);
			}

		/* ---------------------------------
		Checando se o diret�rio de m�dias
		existe (ou foi criado)
		--------------------------------- */
		if(!is_dir($dir_images))
			{
				$status = -3;
			}

		if($status==0)
			{
				
				$cod 	= get($_POST['cod']);
				$nome 	= get($_POST['nome']);
				$mime 	= get($_POST['mime']);

				$mime_type = explode('/',$mime);
				$mime_file = $mime_type[0];
				
				if($mime_file=='image' || $mime_file=='video')
					{

						$extensao_check = explode('.',$nome);
						$extensao = end($extensao_check);
						
						$original_file = $dir_upload.$cod;
						$final_file_dir = $dir_images.$cod;

						$check_copy = copy($original_file, $final_file_dir);

						if(!$check_copy)
							{
								$status	= -4;
							}
					}
			}
	}

/* ---------------------------------
Inserindo no BD
--------------------------------- */
if($status==0)
	{

		$sql = "Insert into midias (arquivo,tipo,id_user) VALUES ('$cod','$mime_file','$id_user')";

		$db = new db;
		$check_insert = $db->insert($sql);

		if($check_insert>0)
			{
			
				$status = 1;
				$content['data'][0]['id'] = $check_insert;
				$content['data'][0]['file'] = $cod;
			
			} else {

				$status = -5;

			}
	}

$content['status'] 	= $status;
$content['message'] = $msg_retorno[$status];
// $content['data'] 	= '';

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

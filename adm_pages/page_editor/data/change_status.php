<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Status atualizado com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualização do conteúdo.';
$msg_retorno[-1] = 'Você não está conectado.';
$msg_retorno[-2] = 'Ocorreu um erro durante a atualização de status da página.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		$page_id = (integer)$_GET['page_id'];
		$page_status = (integer)$_GET['page_status'];
		
		$db = new db;
		$check_update = $db -> query_exec("Update page set ativo='$page_status' where id='$page_id'");
		if(!$check_update)
			{
				$status = -2;
			} else {
				$status = 1;
			}
		
	}

$content['status'] 	= $status;
$content['message'] = $msg_retorno[$status];
$content['data'] 	= '';

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados carregados com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualização de conteúdo.';
$msg_retorno[-1] = 'Você não está conectado.';
$msg_retorno[-2] = 'Ocorreu um erro durante a atualização de dados.';
$msg_retorno[-3] = 'Ocorreu um erro durante o recebimento de dados.';

/* ---------------------------------
Usuário desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usuário logado
--------------------------------- */
if(adm_logged())
	{

		if(!isset($_GET['id']))
			{
				$status = -3;
			}

		if(isset($_GET['id']))
			{
				$id = (integer)$_GET['id'];

				$db = new db;
				$res = $db->query_exec("Update midias set ativo='0' where id='$id'");

				if($res)
					{
						$status = 1;
					} else {
						$status = -2;
					}
			}
	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

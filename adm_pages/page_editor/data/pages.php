<?php
include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 1;
$content['message'] = '';
$content['data'] = '';

/* ---------------------------------
GET variáveis de filtros
--------------------------------- */
$vars = array('id','client','tipo','ativo','q');
foreach ($vars as $key => $value)
	{

		if(isset($_GET[$value]))
			{
				$$value = get($_GET[$value]);
			} else {
				$$value = "";
			}
	}

/* ---------------------------------
SQL construct
--------------------------------- */
$sql = "Select
			p.id as id,
			p.title as title,
			p.tag as tag,
			p.meta_description as meta_description,
			p.url_img_final as url_img_final,
			p.width as width,
			p.margin_bottom as margin_bottom,
			p.ativo as ativo,
			p.data as data,
			p.data_update as data_update,
			c.id as cliente_id,
			c.cliente as cliente_nome,
			t.id as tipo_id,
			t.tipo as tipo
		from
			page as p
			left join page_clientes as c on (c.id=p.id_cliente)
			left join page_tipo as t on (t.id=p.id_tipo)
		where
			p.id > 0";

if($id!='')
	{
		$sql.= " and p.id='$id'";
	}

if($client!='')
	{
		$sql.= " and p.id_cliente='$client'";
	}

if($tipo!='')
	{
		$sql.= " and p.id_tipo='$tipo'";
	}

if($q!='')
	{
		$sql.= " and (p.title like '%$q%' or p.content like '%$q%')";
	}

$sql.= " order by p.data_update desc";

/* ---------------------------------
Query exec
--------------------------------- */
$db = new db;
$page = $db->load($sql);

foreach ($page as $key => $value)
	{

		$id_page = $value['id'];

		/* ---------------------------------
		Montando as views desta página
		--------------------------------- */
		$sql_views = "Select
							p.id as id,
							p.id_view as view_id,
							v.cod as view_cod,
							v.titulo as view_titulo
						from
							page_views as p
							left join view as v on (v.id=p.id_view)
						where
							p.id_page='$id_page'
						order by
							p.ordem asc,
							p.id asc";

		$views = $db->load($sql_views);

		$page[$key]['views'] = $views;

		/* ---------------------------------
		Setando o breadcrumb de páginas
		--------------------------------- */
		$breadcrumb = array();

		$i = 0;

		$breadcrumb_fliped = array_reverse($breadcrumb);
		$page[$key]['breadcrumb'] = $breadcrumb_fliped;

		$breadcrumb_url = '';
		foreach ($breadcrumb_fliped as $key_bread => $value_bread)
			{

				if($breadcrumb_url!='')
					{
						$breadcrumb_url.=' / ';
					}

				$breadcrumb_url.=$value_bread['titulo'];

			}

		$page[$key]['breadcrumb_url'] = $breadcrumb_url;

	}

$content['data'] = json_encode_utf8($page);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

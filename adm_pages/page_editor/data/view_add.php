<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Conteúdo inserido com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a inserção da página.';
$msg_retorno[-1] = 'Você não está conectado.';
$msg_retorno[-2] = 'Erro no recebimento de informações da página.';
$msg_retorno[-3] = 'Ocorreu um erro durante a inserção no Banco de Dados.';

/* ---------------------------------
Usuário desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usuário logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_GET['id_page']) && isset($_GET['id_view']))
			{
				$id_page = (integer)$_GET['id_page'];
				$id_view = (integer)$_GET['id_view'];
			} else {
				$status = -2;
			}

		if($status==0)
			{
				
				$sql_insert = "Insert into page_views (id_page,id_view,ordem,data) VALUES ('$id_page','$id_view','999',now())";

				$db = new db;
				$result = $db->insert($sql_insert);

				if($result>0)
					{

						$content['data'][0]['id'] = $result;
						$content['data'][0]['id_page'] = $id_page;
						$content['data'][0]['id_view'] = $id_view;

						$status = 1;
						
					} else {
						$status = -3;
					}
			}
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
$content['data'] 	= json_encode_utf8($content['data']);

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

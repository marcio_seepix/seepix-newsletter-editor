<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Conte�do ordenado com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a altera��o de ordem de conte�dos.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Erro no recebimento de informa��es da p�gina.';
$msg_retorno[-3] = 'O �tem que voc� est� ordenando n�o est� mais dispon�vel.';
$msg_retorno[-4] = 'Ocorreu um erro durante a altera��o de ordem deste conte�do.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Checando vari�veis
--------------------------------- */
if(!isset($_GET['id']) || !isset($_GET['mov']))
	{
		$status = -2;
	}

if($status==0)
	{
		
		$id = (integer)$_GET['id'];
		$mov = get($_GET['mov']);

		$db = new db;
		$actual_item = $db->load("Select * from page_views where id='$id'");

		if(sizeof($actual_item)==0)
			{
				$status = -3;	
			}
	}

if($status==0)
	{

		$id_page = $actual_item[0]['id_page'];
		$ordem = $actual_item[0]['ordem'];

		/* ---------------------------------
		Atualizando a ordem caso esteja zerada
		--------------------------------- */
		$itens = $db->load("Select * from page_views where id_page='$id_page' order by ordem asc, id asc");
		$i=0;
		foreach ($itens as $key => $value)
			{

				$id_item = $value['id'];
				$db -> query_exec("Update page_views set ordem='$i' where id='$id_item'");
				$i++;
			}

		/* ---------------------------------
		Movendo para cima
		--------------------------------- */
		if($mov=='up')
			{

				$prev_item = $db->load("Select * from page_views where id_page='$id_page' and ordem<'$ordem' order by ordem desc limit 0,1");
				if(sizeof($prev_item)>0)
					{

						$id_prev = $prev_item[0]['id'];
						$db -> query_exec("Update page_views set ordem=(ordem+1) where id='$id_prev'");
						$db -> query_exec("Update page_views set ordem=(ordem-1) where id='$id'");
					}

				$status=1;
			}

		/* ---------------------------------
		Movendo para baixo
		--------------------------------- */
		if($mov=='down')
			{
				$next_item = $db->load("Select * from page_views where id_page='$id_page' and ordem>'$ordem' order by ordem asc limit 0,1");
				if(sizeof($next_item)>0)
					{

						$id_next = $next_item[0]['id'];
						$db -> query_exec("Update page_views set ordem=(ordem-1) where id='$id_next'");
						$db -> query_exec("Update page_views set ordem=(ordem+1) where id='$id'");

					}
					
				$status=1;
			}
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
// $content['data'] 	= '';

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../../config/config.php');
include_once('../../../adm/class/db.class.php');
include_once('../../../adm/php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados carregados com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualiza��o de conte�do.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'N�o foi informado o tipo de m�dia.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(!isset($_GET['type']))
			{
				$status = -2;
			} else {
				$type = $_GET['type'];
			}

		if($type=='img'){ $type='image'; }

		$db = new db;
		$dados = $db->load("Select id,arquivo,tipo from midias where ativo='1' and tipo='$type' order by data_update desc");
		$response['data'] = json_encode_utf8($dados);

		$status = 1;

	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

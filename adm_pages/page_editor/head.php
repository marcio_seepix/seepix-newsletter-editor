<script type="text/javascript" src="../adm/semantic/semantic.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/js/jquery-tablesort.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/components/upload_html5/js/SimpleAjaxUploader.min.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/components/upload_html5/js/upload.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/js/functions/replaceStr.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/js/functions/fullscreen.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm/ckeditor/ckeditor.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../swiper/js/swiper.min.js?v=<?php print $news_version; ?>"></script>
<link rel="stylesheet" type="text/css" href="../swiper/css/swiper.min.css?v=<?php print $news_version; ?>">
<link rel="stylesheet" type="text/css" href="../adm_pages/page_editor/assets/css/page_editor.css?v=<?php print $news_version; ?>">
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor_edit.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor_list.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor_midias.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor_new.js?v=<?php print $news_version; ?>"></script>
<script type="text/javascript" src="../adm_pages/page_editor/assets/js/page_editor_views_edit.js?v=<?php print $news_version; ?>"></script>

<script>
$(function(){

	var hash = location.hash;
	hash = hash.substring(1, hash.length);

	if(hash!=''){
			page_editor_edit(hash);
		} else {
			page_editor_ini();
		}

	})
</script>
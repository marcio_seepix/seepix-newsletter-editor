/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Abrindo o formulário para cadastro
	de uma nova página
	--------------------------------- */
	$(document).on("click","#btn-page-editor-add",function()
		{
			$('#dv-page-editor-new-form').modal('show');
		});

	/* ---------------------------------
	Clique no icone para editar uma pagina
	--------------------------------- */
	$(document).on("click",".btn-page-editor-list-item-edit",function()
		{
			var id_item = $(this).attr('data-id');
			location.hash = id_item;
			page_editor_edit();
		});

	/* ---------------------------------
	LIST > REMOVE > MODAL > OPEN
	--------------------------------- */
	$(document).on("click",".btn-page-editor-list-item-del",function()
		{
			var id_item = $(this).attr('data-id');
			$("#dv-page-editor-page-remove").attr('data-id',id_item);
			$("#dv-page-editor-page-remove").modal('show');
		});

	/* ---------------------------------
	LIST > REMOVE > MODAL > CANCEL
	--------------------------------- */
	$(document).on("click","#btn-page-editor-page-remove-cancel",function()
		{
			$("#dv-page-editor-page-remove").modal('hide');
		});	

	/* ---------------------------------
	LIST > REMOVE > MODAL > CONFIRM
	--------------------------------- */
	$(document).on("click","#btn-page-editor-page-remove-confirm",function()
		{
			loading();
			
			var id_item = $("#dv-page-editor-page-remove").attr('data-id');
			var url_del = '../adm_pages/page_editor/data/page_del.php?id_del='+id_item;

			$.getJSON(url_del,function(res)
				{
					msg = new message();
					if(res.status=='1')
						{
							msg.add('<i class="icon check green"></i> '+res.message);
						} else {
							msg.add('<i class="icon ban red"></i> '+res.message);
						}

					$("#dv-page-editor-page-remove").modal('hide');
					page_editor_ini();
					loading_out();

				})
		});	

	/* ---------------------------------
	DUPLICATE > MODAL > OPEN
	--------------------------------- */
	$(document).on("click",".btn-page-editor-list-item-duplicate",function()
		{
			var id = $(this).attr('data-id');
			$('#page-editor-list-duplicate-modal').modal('show');
			$("#page-editor-list-duplicate-modal").attr('data-id',id);
		});	

	/* ---------------------------------
	DUPLICATE > MODAL > CONFIRM
	--------------------------------- */
	$(document).on("click","#btn-page-editor-list-duplicate-modal-confirm",function()
		{
			loading();
			$('#page-editor-list-duplicate-modal').modal('hide');
			var page_id = $("#page-editor-list-duplicate-modal").attr('data-id');

			var url = '../adm_pages/page_editor/data/duplicate.php?page_id='+page_id;
			$.getJSON(url,function(res)
				{

					if(res.status==1)
						{
							msg = new message();
							msg.add('<i class="icon check green"></i> '+res.message);
							page_editor_ini();
						} else {
							msg = new message();
							msg.add('<i class="icon ban red"></i> '+res.message);
						}
					
					loading_out();
					$('#page-editor-list-duplicate-modal').modal('hide');
					$("#page-editor-list-duplicate").attr('data-id','');
				})

		});	
	
	/* ---------------------------------
	DUPLICATE > MODAL > CANCEL
	--------------------------------- */
	$(document).on("click","#btn-page-editor-list-duplicate-modal-cancel",function()
		{
			$('#page-editor-list-duplicate-modal').modal('hide');
			$("#page-editor-list-duplicate-modal").attr('data-id','');
		});	

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	function page_editor_list_ini()
		{
			console.log('Iniciando a lista');
			page_editor_list_base_load()
			.then(page_editor_clients_load)
			.then(page_editor_list_clients_filter_mount)
			.then(page_editor_publication_type_load)
			.then(page_editor_list_publication_type_filter_mount)
			.then(page_editor_list_activate_events)
			.then(page_editor_list_content_load)
			.then(function()
			{
				console.log('FIM');
			})
		}

	/* ---------------------------------
	Retornando a base do editor
	--------------------------------- */
	function page_editor_list_base_load()
		{

			location.hash = '';
			console.log('Carregando a base da lista');
			var v = page_editor_version_get();
			return get('../adm_pages/page_editor/views/page_editor_list.html?v='+v)
			.then(function(res){
				page_editor_stage.innerHTML = res;
			})
		}

	/* ---------------------------------
	Montando o filtro de clientes
	--------------------------------- */
	function page_editor_list_clients_filter_mount()
	    {
	    	console.log('page_editor_list_clients_filter_mount()');
	        return new Promise((resolve, reject) => 
	        {
	        	$("#cp-page-editor-filter-client").html('<option value="-1">-- Selecione --</option>');
				$("#cp-page-editor-new-client").html('<option value="-1">-- Selecione --</option>');
	        	$.each(page_editor.clients, function(key,value) 
					{
						$('<option value="'+value.id+'">'+value.cliente+'</option>').appendTo("#cp-page-editor-filter-client");
						$('<option value="'+value.id+'">'+value.cliente+'</option>').appendTo("#cp-page-editor-new-client");
					});

	            resolve();
	        })
	    }

	/* ---------------------------------
	Montando o filtro de tipos de publicação
	--------------------------------- */
	function page_editor_list_publication_type_filter_mount()
	    {
	    	console.log('page_editor_list_publication_type_filter_mount()');
	        return new Promise((resolve, reject) => 
	        {
	        	$("#cp-page-editor-filter-type-page").html('<option value="-1">-- Selecione --</option>');
				$("#cp-page-editor-new-tipo-page").html('<option value="-1">-- Selecione --</option>');
	        	$.each(page_editor.publication_type, function(key,value) 
					{
						$('<option value="'+value.id+'">'+value.tipo+'</option>').appendTo("#cp-page-editor-filter-type-page");
						$('<option value="'+value.id+'">'+value.tipo+'</option>').appendTo("#cp-page-editor-new-tipo-page");
					});

	            resolve();
	        })
	    }

	function page_editor_list_activate_events()
		{

			const page_editor_filter_client = document.getElementById('cp-page-editor-filter-client');
			const page_editor_filter_type = document.getElementById('cp-page-editor-filter-type-page');
			const page_editor_filter_search_btn = document.getElementById('cp-page-editor-search');

			page_editor_filter_client.addEventListener("change", page_editor_list_content_load);
			page_editor_filter_type.addEventListener("change", page_editor_list_content_load);
			page_editor_filter_search_btn.addEventListener("click", page_editor_list_content_load);
		}

	/* ---------------------------------
	Listando as páginas existentes
	--------------------------------- */
	function page_editor_list_content_load()
		{

			console.log('page_editor_list_content_load()');

			loading();

			var client 	= $("#cp-page-editor-filter-client").val();
			var tipo 	= $("#cp-page-editor-filter-type-page").val();
			var q 		= $("#cp-page-editor-search").val();

			var url_paginas = '../adm_pages/page_editor/data/pages.php?';
			if(client!='-1'){
					url_paginas = url_paginas+'&client='+client;
				}

			if(tipo!='-1'){
					url_paginas = url_paginas+'&tipo='+tipo;
				}

			if(q!='')
				{
					url_paginas = url_paginas+'&q='+q;
				}

			/* ---------------------------------
			Buscando da API de páginas
			--------------------------------- */
			$.getJSON(url_paginas,function(lista)
				{

					if(lista.data.length==0)
						{
							$("#dv-page-editor-list-blank").removeClass('hide');
							$("#tb-page-editor-list").addClass('hide');
						}

					if(lista.data.length > 0)
						{

							$("#dv-page-editor-list-blank").addClass('hide');
							$("#tb-page-editor-list").removeClass('hide');
							$("#tb-page-editor-list-stage").html('');

							$.each(lista.data,function(key,value)
								{

							  		var cod = '<tr>\
							  						<td>\
														<div data-id="'+value.id+'" class="btn-page-editor-list-item-del ui icon button red small">\
															<i class="icon trash"></i>\
														</div>\
													</td>\
													<td data-sort-value="'+value.id+'">'+value.id+'</td>\
													<td data-sort-value="'+value.cliente_nome+'">'+value.cliente_nome+'</td>\
													<td data-sort-value="'+value.title+'">\
														<p class="font-size-4 bold">'+value.title+'</p>\
													</td>\
													<td data-sort-value="'+value.tipo+'">'+value.tipo+'</td>\
													<td data-sort-value="'+value.data_update+'">'+value.data_update+'</td>\
													<td>\
														<div data-id="'+value.id+'" class="btn-page-editor-list-item-duplicate ui icon button small">\
															<i class="icon copy"></i>\
														</div>\
													</td>\
													<td>\
														<div data-id="'+value.id+'" class="btn-page-editor-list-item-edit ui icon button small">\
															<i class="icon write"></i>\
														</div>\
													</td>\
												</tr>';

									$(cod).appendTo("#tb-page-editor-list-stage");

								});

							$('table').tablesort();

						}

					loading_out();

				})

		}
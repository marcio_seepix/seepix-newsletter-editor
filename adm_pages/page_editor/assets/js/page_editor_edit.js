/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Limpando o intervalo ao passar o mouse
	sobre o hover
	--------------------------------- */
	$(document).on("mouseenter","#dv-view-editor-hover",function()
		{
			if(typeof(interval_view_editor_out)!='undefined')
				{
					clearInterval(interval_view_editor_out);
				}
		});

	/* ---------------------------------
	Mouse leave do editor hover
	--------------------------------- */
	$(document).on("mouseleave","#dv-view-editor-hover",function()
		{
			interval_view_editor_out = setTimeout(function()
				{
					view_editor_fechar_hover();
				}, 800);
		});

	/* ---------------------------------
	Mouse over sobre a view na página
	--------------------------------- */
	$(document).on("mouseenter",".dv-page-editor-view-list-item",function()
		{

			if(typeof(interval_view_editor_out)!='undefined')
				{
					clearInterval(interval_view_editor_out);
				}

			/* ---------------------------------
			Posição do elemento
			--------------------------------- */
			var elem = $(this);
			var id = $(this).attr('data-id');
			var view_id = $(this).attr('data-view_id');
			var view_cod = $(this).attr('data-view_cod');

			$("#dv-view-editor-hover").attr('data-id',id);
			$("#dv-view-editor-hover").attr('data-view_id',view_id);
			$("#dv-view-editor-hover").attr('data-view_cod',view_cod);

			var pos = elem.offset();
			var w = elem.width();
			var h = elem.height();
			var x = pos.left;
			var y = pos.top;

			/* ---------------------------------
			Posição dos elementos pai
			--------------------------------- */
			var elem_pai = $("#dv-page-editor-main");
			var pos_pai = elem_pai.offset();
			var x_pai = pos_pai.left;
			var y_pai = pos_pai.top;

			x = x - x_pai;
			y = y - y_pai;

			var elem_pai = $("#dv-page-editor-edit-view-list-block");
			var pos_pai = elem_pai.offset();
			var x_pai = pos_pai.left;
			var y_pai = pos_pai.top;

			x = x-20;
			y = y-65;

			w = w + 20;
			h = h + 20;

			editor = $("#dv-view-editor-hover");
			editor.css('width',w+'px');
			editor.css('height',h+'px');
			editor.css('top',y+'px');
			editor.css('left',x+'px');
			editor.css('opacity','1');
			editor.css('display','block');

		});

	/* ---------------------------------
	Mouse leave da view
	--------------------------------- */
	$(document).on("mouseleave",".dv-page-editor-view-list-item",function()
		{

			interval_view_editor_out = setTimeout(function()
				{
					view_editor_fechar_hover();
				}, 300);

		});

	/* ---------------------------------
	VIEW > EDIT ICON > CLICK 
	--------------------------------- */
	$(document).on("click","#btn-view-content-editar",function()
		{
			page_editor_view_content_edit();
		});

	/* ---------------------------------
	VIEW > HOVER EFFECT > CLICK 
	--------------------------------- */
	$(document).on("click","#dv-view-editor-hover-stage",function()
		{
			page_editor_view_content_edit();
		});

	/* ---------------------------------
	Clique no botão para remover uma view
	--------------------------------- */
	$(document).on("click","#btn-view-content-excluir",function()
		{

			var id = $("#dv-view-editor-hover").attr('data-id');
			var view_id = $("#dv-view-editor-hover").attr('data-view_id');
			var view_cod = $("#dv-view-editor-hover").attr('data-view_cod');
			var id_page = view_editor_page_id_get();

			page_editor_view_remove(id_page,view_id,id);

		});

	/* ---------------------------------
	Cancelar o modal de confirmação
	de exclusão	de view
	--------------------------------- */
	$(document).on("click","#btn-page-editor-view-confirm-remove-cancel",function()
		{
			$("#dv-page-editor-view-confirm-remove").modal('hide');
		});

	/* ---------------------------------
	Confirmando a remoção de uma view
	--------------------------------- */
	$(document).on("click","#btn-page-editor-view-confirm-remove-confirm",function()
		{
			page_editor_view_remove_confirm();
		});

	/* ---------------------------------
	Clique no botão fechar no modal de
	seleção de nova view
	--------------------------------- */
	$(document).on("click","#icon-page-editor-add-content-close",function()
		{
			page_editor_add_content_close();
		});

	/* ---------------------------------
	Clique no background do formulário
	de edição de conteúdo
	--------------------------------- */
	$(document).on("click","#dv-page-editor-content-form-background",function()
		{
			page_editor_view_content_editor_close();
		});

	/* ---------------------------------
	Clique no botão de cancelar no formulário
	de edição de conteúdo da view
	--------------------------------- */
	$(document).on("click","#btn-page-editor-content-form-cancel",function()
		{
			page_editor_view_content_editor_close();
		});

	/* ---------------------------------
	Clique no botão salvar no formulário
	de edição de conteúdo da view
	--------------------------------- */
	$(document).on("click","#btn-page-editor-content-form-confirm",function()
		{
			page_editor_view_content_save();
		});

	/* ---------------------------------
	Salvando a página
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-salvar",function()
		{
			page_editor_save();
		});

	/* ---------------------------------
	Clique no icone de remoção de vídeo
	no formulário do campo da view
	--------------------------------- */
	$(document).on("click",".btn-page-editor-field-video-remove",function()
		{
			var cod = $(this).attr('data-view_field');
			$(".page_editor_content_field[data-view_field="+cod+"]").val();
			$(".page_editor_content_field_video[data-view_field="+cod+"]").html('');
		});

	/* ---------------------------------
	Clique no icone de remoção de imagem
	no formulário do campo da view
	--------------------------------- */
	$(document).on("click",".btn-page-editor-field-img-remove",function()
		{
			var cod = $(this).attr('data-view_field');
			$(".page_editor_content_field[data-view_field="+cod+"]").val();
			$(".page_editor_content_field_img[data-view_field="+cod+"]").css('background-image','');
		});

	/* ---------------------------------
	Clique no botão de enviar mídia no
	formulário de edição do conteúdo
	--------------------------------- */
	$(document).on("click",".btn-page-editor-field-upload-midia",function()
		{
			view_field = $(this).attr('data-view_field');
			view_type = $(this).attr('data-view_type');

			var config_upload_ini = {};
			config_upload_ini.field = view_field;
			config_upload_ini.type = view_type;
			config_upload_ini.modo = 'view';

			page_editor_uploads_init(config_upload_ini);

		});

	/* ---------------------------------
	EDITOR > DOWNLOAD > MODAL > OPEN
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-download",function()
		{
			$("#dv-page-editor-edit-download-modal").removeClass('hide');
		});	

	/* ---------------------------------
	EDITOR > DOWNLOAD > MODAL > CLOSE
	Mouse leave sobre o modal de download
	e alteração de url de imagens
	--------------------------------- */
	$(document).on("mouseleave","#dv-page-editor-edit-download-modal",function()
		{
			$("#dv-page-editor-edit-download-modal").addClass('hide');
		});	

	/* ---------------------------------
	EDITOR > DOWNLOAD > EXEC
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-download-exec",function()
		{
			page_editor_download_zip();
		});	

	/* ---------------------------------
	Ver o código HTML
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-cod-html",function()
		{
			page_editor_edit_collect_html();			
			$("#dv-page-editor-edit-cod-html-stage").removeClass('hide');
		});	

	/* ---------------------------------
	Fechando o modal de visualização 
	do código HTML
	--------------------------------- */
	$(document).on("click","#dv-page-editor-edit-cod-html-stage-btn-close",function()
		{
			$("#dv-page-editor-edit-cod-html-stage").addClass('hide');
		});	

	/* ---------------------------------
	Alterando o endereço de imagens do
	código HTML
	--------------------------------- */
	$(document).on("click","#btn-page-editor-url-img-to-save",function()
		{
			page_editor_cod_html_change_url_img();		
		});	

	/* ---------------------------------
	Alternando para fullscreen
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-fullscreen",function()
		{
			fullscreen();
		});	

	/* ---------------------------------
	Ordenando views - Enviando para cima
	--------------------------------- */
	$(document).on("click","#btn-view-content-position-up",function()
		{

			var id = $("#dv-view-editor-hover").attr('data-id');
			page_editor_views_sort(id,'up');

		});

	/* ---------------------------------
	Ordenando views - Enviando para baixo
	--------------------------------- */
	$(document).on("click","#btn-view-content-position-down",function()
		{

			var id = $("#dv-view-editor-hover").attr('data-id');
			page_editor_views_sort(id,'down');

		});

	/* ---------------------------------
	Ajustando a largura do conteúdo
	--------------------------------- */
	$(document).on("change","#cp-page-editor-edit-width",function()
		{
			page_editor_set_width();
		});

	/* ---------------------------------
	Ajustando a margem bottom do conteúdo
	--------------------------------- */
	$(document).on("change","#cp-page-editor-edit-margin-bottom",function()
		{
			page_editor_set_margin_bottom();
		});

	/* ---------------------------------
	Abrindo o preview da página
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-preview",function()
		{
			page_editor_preview();
		});	

	/* ---------------------------------
	Fechando o preview da página
	--------------------------------- */
	$(document).on("click","#icon-page-editor-preview-close",function()
		{
			page_editor_overlay_close();
		});	

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Editando uma página
	--------------------------------- */
	function page_editor_edit()
		{
			loading();
			page_editor_edit_load_core()
			.then(page_editor_edit_content_load)
			.then(page_editor_edit_content_mount)
			.then(page_editor_views_load)
			.then(page_editor_start_events)
			.then(function(){

				loading_out();
				console.log('--------- FIM ---------');
			})
		}

	function page_editor_edit_get_id()
		{
			var hash = location.hash;
			hash = replaceStr(hash,"#","");
			return hash;
		}

	function page_editor_edit_load_core()
		{
			console.log('page_editor_edit_load_core()');
			var timestamp = new Date().getTime();
			return get('../adm_pages/page_editor/views/page_editor_edit.html?t_='+timestamp)
				.then(function(res){
					var admPagesStage = document.getElementById('adm-pages-stage');
					admPagesStage.innerHTML = res;
				})
		}

	function page_editor_edit_content_load()
		{

			console.log('page_editor_edit_content_load()');
			var timestamp = new Date().getTime();
			var id = page_editor_edit_get_id();

			return get('../adm_pages/page_editor/data/pages.php?id='+id+'&t_='+timestamp)
				.then(function(res){
					var data = JSON.parse(res);
					page_editor.page_content = data.data[0];
				})
		}

	function page_editor_edit_content_mount()
		{

			console.log('page_editor_edit_content_mount()');

			var content = page_editor.page_content;

			$("#dv-page-editor").attr('data-page_id',content.id);
			$("#cp-page-editor-edit-title").val(content.title);
			$("#cp-page-editor-url-img-to-save").val(content.url_img_final);
			$("#cp-page-editor-edit-width").val(content.width);

			page_editor.page_content.views.map(function(value,key)
				{
					var cod_append = '<div class="dv-page-editor-view-list-item-loading dv-page-editor-view-list-item" data-id="'+value.id+'" data-view_id="'+value.view_id+'" data-view_cod="'+value.view_cod+'"><i class="icon spinner loading"></i></div>';
					$(cod_append).appendTo("#dv-page-editor-edit-view-list");
				})

			page_editor_set_width();
			page_editor_edit_stage_view_loading();

		}

	function page_editor_views_load()
		{

			console.log('page_editor_views_load()');

			return get('../view/view.php')
			.then(function(res){
				res = JSON.parse(res);
				page_editor.views = res.data;
			})
		}

	function page_editor_start_events()
		{

			console.log('page_editor_start_events()');

			$('.adm_popup').popup();

			/* voltar à página inicial */
			document.getElementById('btn-page-editor-edit-back').addEventListener('click', page_editor_ini);

			/* Adicionar um novo conteúdo */
			document.getElementById('btn-page-editor-edit-add-content').addEventListener('click', page_editor_add_content_open);

		}

	/* ---------------------------------
	Abrindo o modal para seleção de novas views
	--------------------------------- */
	function page_editor_add_content_open()
		{
			console.log('page_editor_add_content_open()');
			return get('../adm_pages/page_editor/views/page_editor_add_content.html')
			.then(function(res){
				$("#dv-page-editor-overlay").html(res);
				$("#dv-page-editor-overlay").css('display','block');
				return true;
			})
			.then(page_editor_add_content_add_views)
			.then(page_editor_add_content_start_events)
		}

	/* ---------------------------------
	Adicionando as views no modal de novo conteúdo
	--------------------------------- */
	function page_editor_add_content_add_views()
		{

			console.log('page_editor_add_content_add_views()');

			var delay = 150;
			var i = 0;
			$.each(page_editor.views, function( key, value )
				{
					
					time_intro = i * delay;
					page_editor_add_content_show_item(value,time_intro);

					i++;
				});
		}

	/* ---------------------------------
	Incluindo um ítem na janela de seleção
	de nova view
	--------------------------------- */
	function page_editor_add_content_show_item(value,delay)
		{

			console.log('page_editor_add_content_show_item()');

			var cod = '<div class="grid-2 margin-b-20 animated fadeInUp" style="-webkit-animation-delay: '+delay+'ms; animation-delay: '+delay+'ms;>\
							<div class="pad-10">\
								<div class="page-editor-add-content-item pad-20" data-view_id="'+value.id+'">\
									<div class="aspect-square bg-auto margin-b-10 shadow" style="background-image:url(../'+value.thumbs+'">\
										<div></div>\
									</div>\
									<p class="bold font-size-4">'+value.titulo+'</p>\
									<p class="grey font-size-2">'+value.description+'</p>\
								</div>\
							</div>\
						</div>';

			$(cod).appendTo("#page-editor-add-content-stage-list")

		}	

	/* ---------------------------------
	Ativa os cliques nas views no modal de 
	adição de novo conteúdo
	--------------------------------- */
	function page_editor_add_content_start_events()
		{
			var elements = document.getElementsByClassName('page-editor-add-content-item');
			for(var i = 0; i < elements.length; i++) {
				elements[i].removeEventListener('click', page_editor_add_content_select_view, true);
				elements[i].addEventListener('click', page_editor_add_content_select_view, true);
				}
		}

	/* ---------------------------------
	Seleciona a view clicada
	--------------------------------- */
	function page_editor_add_content_select_view(ev)
		{
			var id = ev.srcElement.dataset.view_id;
			page_editor_add_content_view_insert(id);
		}

	/* ---------------------------------
	Adicionando uma view à página atual
	--------------------------------- */
	function page_editor_add_content_view_insert(id_view)
		{

			loading();
			var id_page = view_editor_page_id_get();
			$.getJSON('../adm_pages/page_editor/data/view_add.php?id_view='+id_view+'&id_page='+id_page,function(res)
				{
					page_editor_add_content_close();
					loading_out();
				})

		}

	/* ---------------------------------
	Fechando o modal de seleção de novas views
	--------------------------------- */
	function page_editor_add_content_close()
		{
			$("#dv-page-editor-overlay").html('');
			$("#dv-page-editor-overlay").css('display','none');

			var id_page = view_editor_page_id_get();
			page_editor_edit(id_page);
		}

	function page_editor_overlay_close()
		{
			$("#dv-page-editor-overlay").html('');
			$("#dv-page-editor-overlay").css('display','none');
		}

	/* ---------------------------------
	Faz o preview da news
	--------------------------------- */
	function page_editor_preview()
		{

			var content = $("#dv-page-editor-edit-view-list").parent().html();
			
			var conteudo_html = get('../adm_pages/page_editor/views/page_editor_preview.html');
			Promise.all([conteudo_html])
				.then(function(res){

					console.log('Res: ',res)

					$("#dv-page-editor-overlay").html(res[0]);
					$("#dv-page-editor-overlay").css('display','block');
					$("#page-editor-preview-stage-content").html(content);

				})
				.catch(function(err){

					msg = new message();
					msg.duration('10');
					msg.add('<i class="icon red ban"></i> Erro no carregamento de conteúdos, tente novamente em instantes ou entre em contato com o suporte técnico do site.');

				});
		}
	
	/* ---------------------------------
	Aplica a largura no preview.
	--------------------------------- */
	function page_editor_set_width()
		{
			var w = $("#cp-page-editor-edit-width").val();
			if(w>0)
				{
					w = parseInt(w)	;
					w_margin = w/2;

					$("#dv-page-editor-edit-view-list").css('width',w+'px');
					$("#dv-page-editor-edit-view-list").css('left','50%');
					$("#dv-page-editor-edit-view-list").css('margin-left','-'+w_margin+'px');
				} else {
					$("#dv-page-editor-edit-view-list").css('width','100%');
				}
		}

	/* ---------------------------------
	Aplica a margem entre views
	--------------------------------- */
	function page_editor_set_margin_bottom()
		{
			var w = $("#cp-page-editor-edit-margin-bottom").val();
			if(w>0)
				{
					w = parseInt(w)	;
					$(".dv-page-editor-view-list-item").css('margin-bottom',w+'px');
				} else {
					$(".dv-page-editor-view-list-item").css('margin-bottom','0px');
				}
		}

	/* ---------------------------------
	Download de ZIP empacotado
	--------------------------------- */
	function page_editor_download_zip()
		{

			loading();

			var cod = $("#cp-page-editor-content-to-save").val();
			var url_img = $("#cp-page-editor-url-img-to-save").val();

			var form = new FormData();
			form.append("cod", cod);
			form.append("url_img", url_img);
			
			var url = '../adm_pages/page_editor/data/download.php';
			var settings = {
			  "async": true,
			  "crossDomain": true,
			  "crossOrigin": 'anonymous',
			  "url": url,
			  "method": "POST",
			  "processData": false,
			  "contentType": false,
			  "mimeType": "multipart/form-data",
			  "data": form
			}
			
			$.ajax(settings).done(function (response) 
				{
					res = JSON.parse(response);
					if(res.status==1)
						{

							var url = res.data;
							location.href=url;

							loading_out();

						} else {
							msg = new message();
							msg.add('<i class="icon red ban"></i> '+res.message);
						}
					
					// loading_out();
				});
		}

	/* ---------------------------------
	Ordenando as views
	--------------------------------- */
	function page_editor_views_sort(id,mov)
		{
			$.getJSON('../adm_pages/page_editor/data/view_sort.php?id='+id+'&mov='+mov,function(response)
				{

					var id_page=view_editor_page_id_get();
					page_editor_edit(id_page);

				})
		}

	/* ---------------------------------
	Salvando a página
	--------------------------------- */
	function page_editor_save()
		{

			loading();

			var id_page = view_editor_page_id_get();
			var status = $("#cp-page-editor-edit-status").val();
			var titulo = $("#cp-page-editor-edit-title").val();
			var secao = $("#cp-page-editor-edit-section").val();
			var url_img_final = $("#cp-page-editor-url-img-to-save").val();
			var width = $("#cp-page-editor-edit-width").val();
			var margin_bottom = $("#cp-page-editor-edit-margin-bottom").val();
			//var content = $("#dv-page-editor-edit-view-list").html();

			var content = $("#cp-page-editor-content-to-save").val();
			content = replaceStr(content,"'","´");

			var form = new FormData();
			form.append("data[id_page]", id_page);
			form.append("data[status]", status);
			form.append("data[titulo]", titulo);
			form.append("data[secao]", secao);
			form.append("data[content]", content);
			form.append("data[url_img_final]", url_img_final);
			form.append("data[width]", width);
			form.append("data[margin_bottom]", margin_bottom);

			var url = '../adm_pages/page_editor/data/page_save.php';
			fetch(url,{
					method: 'POST',
					mode: 'no-cors',
					credentials: "same-origin",
					body:form
				})
				.then(function(response){

					if(response.ok)
						{
							return response.json();
						} else{
							msg = new message();
							msg.duration(5);
							msg.add('<i class="icon ban red"></i> Ocorreu um erro durante o carregamento desta página');
							loading_out();
						}

				})
				.then(function(data){

					msg = new message();
					if(data.status=='1')
						{
							msg.add('<i class="icon check green"></i> '+data.message);
						} else{
							msg.add('<i class="icon ban red"></i> '+data.message);
						}

					loading_out();

				})
				.catch(function(err){
					console.log(err);
				})

		}

	/* ---------------------------------
	Salvando o conteúdo editado da view
	--------------------------------- */
	function page_editor_view_content_save()
		{

			id_content = view_editor_form_content_id_view_get();
			page_id = view_editor_page_id_get();
			view_id = view_editor_form_id_view_get();

			var lista = '';
			data_send = [];

			i = 0;
			$.each($(".page_editor_content_field"), function(key,value)
				{

					var field = $(this).attr('data-view_field');
					var type = $(this).attr('data-view_type');
					var content = $(this).val();

					if(lista!='')
						{
							lista+='&';
						}

					lista+= 'config['+i+'][value]='+escape(content);
					lista+= '&config['+i+'][page_id]='+escape(page_id);
					lista+= '&config['+i+'][content_view_id]='+escape(id_content);
					lista+= '&config['+i+'][view_id]='+escape(view_id);
					lista+= '&config['+i+'][view_field_cod]='+escape(field);
					lista+= '&config['+i+'][view_field_type]='+escape(type);

					i++;

				});

			loading();

			var url = '../adm_pages/page_editor/data/content_save.php';
			$.post(url,lista,function(response){

					msg = new message();
					if(response.status==1)
						{
							msg.add('<i class="icon check green"></i> '+response.message);

							page_editor_edit(page_id);

						} else {
							msg.add('<i class="icon red ban"></i> '+response.message);
						}

					loading_out();
					page_editor_view_content_editor_close();

				},"json");


		}

	/* ---------------------------------
	Editando um conteúdo de uma view
	--------------------------------- */
	function page_editor_view_content_edit()
		{

			var content_view_id = $("#dv-view-editor-hover").attr('data-id');
			var view_id = $("#dv-view-editor-hover").attr('data-view_id');
			var view_cod = $("#dv-view-editor-hover").attr('data-view_cod');
			var page_id = view_editor_page_id_get();

			// var page_id = view_editor_page_id_get();
			// var content_view_id = config['id'];
			// var view_id = config['view_id'];
			// var view_cod = config['view_cod'];

			$("#dv-page-editor-content-form").attr('data-id_content_view',content_view_id);
			$("#dv-page-editor-content-form").attr('data-id_view',view_id);

			$("#dv-page-editor-content-form").removeClass('hide');
			$("#dv-page-editor-content-form-stage").removeClass('slideOutLeft').addClass('slideInLeft');

			var dados_form = get('../view/view.php?id='+view_id+'&id_page='+page_id);
			var content = get('../adm_pages/page_editor/data/content.php?content_view_id='+content_view_id);

			Promise.all([dados_form,content])
				.then(function(res){

				    /* ---------------------------------
				    Montando os campos do form
				    --------------------------------- */
				    var dados_form = JSON.parse(res[0]);
				    if(dados_form.status==1)
					    {
					    	var fields = dados_form.data[0].fields;

						    $("#dv-page-editor-content-form-stage-fields").html('');
						    $.each(fields, function(key,value)
						    	{
									page_editor_view_content_editor_create_field(value);
						    	});

						    /* ---------------------------------
						    Setando os valores do form
						    --------------------------------- */
						    var content = JSON.parse(res[1]);
						    $.each(content.data, function(key,value)
						    	{
						    		page_editor_view_content_set_value(value);
						    	});

						    /* ---------------------------------
						    Setando os ckeditors dos textareas
						    --------------------------------- */
						    var total_textareas = $('.page_editor_content_field[data-view_type=longtext]').length;
						    if(total_textareas>0)
							    {
							    	for (var i = 0; i < total_textareas; i++) {
							    			var textarea_id = $('.page_editor_content_field[data-view_type=longtext]').eq(i).attr('id');
							    			page_editor_ckeditor_ini(textarea_id);
							    		}
							    }
					    } else {

					    	msg = new message();
					    	msg.add('<i class="icon red ban"></i> '+dados_form.message);

					    }

				})
				.catch(function(err){
				    console.log('Erro no carregamento de dados', err);
				});

		}

	/* ---------------------------------
	Setando um valor no formulário de
	edição de conteúdo da view
	--------------------------------- */
	function page_editor_view_content_set_value(config)
		{

			$(".page_editor_content_field[data-view_field="+config['view_field_cod']+"]").val(config['value']);
			if(config.view_field_type=='img')
				{
					$(".page_editor_content_field_img[data-view_field="+config['view_field_cod']+"]").css('background-image','url('+config['value']+')');
				}

			if(config.view_field_type=='video')
				{
					$(".page_editor_content_field_video[data-view_field="+config['view_field_cod']+"]").html('<video width="100%" height="100%" controls><source src="'+config['value']+'" type="video/mp4"></video>');
				}
		}

	/* ---------------------------------
	Criando o campo no formulário de
	edição de conteúdo
	--------------------------------- */
	function page_editor_view_content_editor_create_field(config_field)
		{

			cod_html = '';

			var view_field_id = config_field['id'];
			var view_cod = config_field['cod'];
			var view_label = config_field['label'];
			var view_type = config_field['type'];

			view_cod = view_cod.trim();
			view_label = view_label.trim();
			view_type = view_type.trim();

			if(view_type=="text")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="text" placeholder="">\
		                </div>';
				}

			if(view_type=="color")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="color" placeholder="">\
		                </div>';
				}

			if(view_type=="number")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="number" placeholder="">\
		                </div>';
				}

			if(view_type=="longtext")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <textarea id="page_editor_content_field_textarea_'+view_field_id+'" class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'"></textarea>\
		                </div>';
				}

			if(view_type=="hiperlink")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="text" placeholder="http://">\
		                </div>';
				}

			if(view_type=="img")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="hidden" placeholder="">\
		                    <div class="aspect-wide bg-grey-light">\
		                    	<div class="page_editor_content_field_img bg-auto" data-view_field="'+view_cod+'" data-view_type="'+view_type+'"></div>\
		                    </div>\
		                    <div class="pad-5 bg-grey-ultralight text-r">\
                    			<div class="btn-page-editor-field-img-remove ui icon button mini red inverted"  data-view_field="'+view_cod+'">\
                    			  <i class="trash icon"></i>\
                    			</div>\
	                    		<div class="btn-page-editor-field-upload-midia ui labeled icon button mini" data-view_field="'+view_cod+'" data-view_type="'+view_type+'">\
	                    		  <i class="photo icon"></i>\
	                    		  Enviar\
	                    		</div>\
	                    	</div>\
		                </div>';
				}

			if(view_type=="video")
				{
					cod_html = '<div class="field">\
		                    <label>'+view_label+':</label>\
		                    <input class="page_editor_content_field" data-view_field="'+view_cod+'" data-view_type="'+view_type+'" type="hidden" placeholder="">\
		                    <div class="aspect-wide bg-grey-light">\
		                    	<div class="page_editor_content_field_video" data-view_field="'+view_cod+'" data-view_type="'+view_type+'">\
		                    		<video width="100%" height="100%" controls>\
									  <source src="" type="video/mp4">\
									</video>\
		                    	</div>\
		                    </div>\
                    		<div class="pad-5 bg-grey-ultralight text-r">\
                    			<div class="btn-page-editor-field-video-remove ui icon button mini red inverted" data-view_field="'+view_cod+'">\
                    			  <i class="trash icon"></i>\
                    			</div>\
	                    		<div class="btn-page-editor-field-upload-midia ui labeled icon button mini" data-view_field="'+view_cod+'" data-view_type="'+view_type+'">\
	                    		  <i class="video icon"></i>\
	                    		  Enviar video\
	                    		</div>\
	                    	</div>\
		                </div>';
				}

			$(cod_html).appendTo("#dv-page-editor-content-form-stage-fields");

		}

	/* ---------------------------------
	Fechando o editor de conteúdo da view
	--------------------------------- */
	function page_editor_view_content_editor_close()
		{
			$("#dv-page-editor-content-form-stage").removeClass('slideInLeft').addClass('slideOutLeft');
			setTimeout(function(){

				/* ---------------------------------
				Encerrando as instâncias do Ckeditor
				--------------------------------- */
				for(name in CKEDITOR.instances)
					{
					    CKEDITOR.instances[name].destroy()
					}

				$("#dv-page-editor-content-form").addClass('hide');
				}, 1000);
		}

	/* ---------------------------------
	Retornando o ID da página atual
	--------------------------------- */
	function view_editor_page_id_get()
		{
			var id_page = $("#dv-page-editor").attr('data-page_id');
			return id_page;
		}

	/* ---------------------------------
	Buscando o id do conteúdo em edição
	--------------------------------- */
	function view_editor_form_content_id_view_get()
		{
			var content_view_id = $("#dv-page-editor-content-form").attr('data-id_content_view');
			return content_view_id;
		}

	/* ---------------------------------
	Buscando o id da view em edição
	--------------------------------- */
	function view_editor_form_id_view_get()
		{
			var view_id = $("#dv-page-editor-content-form").attr('data-id_view');
			return view_id;
		}

	/* ---------------------------------
	Fechar o hover de ações sobre as views
	--------------------------------- */
	function view_editor_fechar_hover()
		{
			editor = $("#dv-view-editor-hover");
			editor.css('opacity','0');

			setTimeout(function()
				{
					editor.css('width','0px');
					editor.css('height','0px');
					editor.css('top','0px');
					editor.css('left','0px');
					editor.css('display','none');
				},1000);
		}

	/* ---------------------------------
	Remover view - Abrir modal de confirmação
	--------------------------------- */
	function page_editor_view_remove(id_page,id_view,id_content)
		{

			$("#dv-page-editor-view-confirm-remove").attr('data-id',id_content);
			$("#dv-page-editor-view-confirm-remove").attr('data-id_page',id_page);
			$("#dv-page-editor-view-confirm-remove").attr('data-id_view',id_view);
			$("#dv-page-editor-view-confirm-remove").modal('show');
		}

	/* ---------------------------------
	Confirmando a remoção de uma view
	--------------------------------- */
	function page_editor_view_remove_confirm()
		{

			loading();

			var id_item = $("#dv-page-editor-view-confirm-remove").attr('data-id');
			var id_page = $("#dv-page-editor-view-confirm-remove").attr('data-id_page');
			var id_view = $("#dv-page-editor-view-confirm-remove").attr('data-id_view');

			$("#dv-page-editor-view-confirm-remove").modal('hide');

			var remove_status = get('../adm_pages/page_editor/data/view_remove.php?id_item='+id_item+'&id_page='+id_page+'&id_view='+id_view);

			Promise.all([remove_status])
				.then(function(res){

				    msg = new message();
				    result = JSON.parse(res[0]);

				    /* ---------------------------------
				    Confirmando a remoção do conteúdo
				    --------------------------------- */
				    if(result.status==1)
					    {

					    	var id_page = view_editor_page_id_get();
					    	page_editor_edit(id_page);
					    	msg.add('<i class="icon check green"></i> Conteúdo removido com sucesso!');

					    } else {

					    	/* ---------------------------------
					    	Ocorreu um erro n aremoção do conteúdo
					    	--------------------------------- */
					    	msg.add('<i class="icon ban red"></i> '+res[0].message);
					    }

				    loading_out();
				})
				.catch(function(err){
				    console.log('Erro no carregamento de dados', err);
				});
		}

	/* ---------------------------------
	Carrega a view dentro do espaço temporário 
	de views no início da edição de uma página
	--------------------------------- */
	function page_editor_edit_stage_view_loading()
		{

			var contar_views = $(".dv-page-editor-view-list-item-loading").length;
			if(contar_views>0)
				{

					var timestamp = new Date().getTime();
					var content_id = $(".dv-page-editor-view-list-item-loading:first").attr('data-id');
					var view_cod = $(".dv-page-editor-view-list-item-loading:first").attr('data-view_cod');
					var view_id = $(".dv-page-editor-view-list-item-loading:first").attr('data-view_id');

					var load_view = get('../view/'+view_cod+'/view.html?t='+timestamp);
					var load_content = get('../adm_pages/page_editor/data/content.php?content_view_id='+content_id+'&t_='+timestamp);
					
					Promise.all([load_view,load_content])
						.then(function(res){

							/* ---------------------------------
							Incluindo conteúdo da view
							--------------------------------- */
							content = JSON.parse(res[1]);
							/* ---------------------------------
							Buscando o ID deste conteúdo
							--------------------------------- */
							content_view_id = 0;
							if(content.data.length>0)
								{
									var content_view_id = content.data[0].content_view_id;
									var view_id = content.data[0].view_id;
									res[0] = replaceStr(res[0],'view-field-content','view-field-alterado view-field-id-register-'+content_view_id);
								} else {
									res[0] = '<div style="border:solid 5px #F60; padding: 10px">'+res[0]+'</div>';
								}

							/* ---------------------------------
						    Carregando a view em HTML
						    --------------------------------- */
						    var view_html_content = res[0];

							$.each(content.data,function(key,value)
								{

									var content_value = value.value;
									var content_type = value.view_field_type;
									var view_id = value.view_id;
									var view_field_cod = value.view_field_cod;

									view_field_cod = view_field_cod.toUpperCase();
									view_html_content = replaceStr(view_html_content,'{{'+view_field_cod+'}}',content_value);

								});

							$(".dv-page-editor-view-list-item-loading:first").html(view_html_content);
							$(".dv-page-editor-view-list-item-loading:first").removeClass('dv-page-editor-view-list-item-loading');
							page_editor_edit_stage_view_loading();

						})
						.catch(function(err){
						    console.log('Erro no carregamento de dados', err);
						});
				} else {
					page_editor_edit_collect_html();
				}
		}

	/* ---------------------------------
	Coletando os dados para o output HTML
	--------------------------------- */
	function page_editor_edit_collect_html()
		{
			var content_width = $("#cp-page-editor-edit-width").val();
			if(content_width=='0' || content_width=='')
				{
					content_width = '100%;';
				} else {
					content_width = content_width+'px';
				}

			var cod_html = '<table width="'+content_width+'" margin="0" padding="0" style="width:'+content_width+'"><tr><td>';
			var count_views = $(".view-editor-item").length;

			var margin_bottom = $("#cp-page-editor-edit-margin-bottom").val();

			$.each( $(".view-editor-item"), function( key, value ) {
			  	
			  	var content = $(this).children('center').html();
			  	if(margin_bottom>0)
			  		{
						cod_html+='<table width="100%" margin="0" padding="0" style="margin-bottom:'+margin_bottom+'px"><tr><td>';
						cod_html+=content;
						cod_html+='</td></tr></table>';
			  		} else {
			  			cod_html+=content;
			  		}

				});

			cod_html+='</td></tr></table>';
			$("#cp-page-editor-content-to-save").val(cod_html);
		}
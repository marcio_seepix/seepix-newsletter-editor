var timestamp = new Date().getTime();

var page_editor = {};
page_editor.clients = {};
page_editor.publication_type = {};
page_editor.page_content = {};
page_editor.views = {};

var page_editor_stage = document.getElementById('adm-pages-stage');

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	/* ---------------------------------
	On press enter na busca
	--------------------------------- */
	$(document).on("keypress","#cp-page-editor-search",function(e)
		{

			if(e.keyCode) { // IE
				tecla = e.keyCode;
			}  else if (e.which) { // Firefox
				tecla = e.which;
			}  else { // Outro
				return false;
			}

			if(tecla==13)
				{
					// função
					page_editor_list_content_load();
				}
		});

	/* ---------------------------------
	On click na busca
	--------------------------------- */
	$(document).on("click","#icon-page-editor-search",function()
		{
			page_editor_list_content_load();
		});

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	function page_editor_version_get()
		{
			var version = $("#dv-page-editor").attr('data-version');
		}
	
	/* ---------------------------------
	Iniciando o editor de páginas
	--------------------------------- */
	function page_editor_ini()
		{
			page_editor_list_ini();
		}

	function page_editor_publication_type_load()
		{
			console.log('page_editor_publication_type_load()');
			return get('../adm_pages/page_editor/data/page_tipo.php?temp='+timestamp)
				.then(function(res){
					res = JSON.parse(res);
					page_editor.publication_type = res.data;
				});
		}

	function page_editor_clients_load()
		{
			console.log('page_editor_clients_load()');
			return get('../adm_pages/page_editor/data/clientes.php?temp='+timestamp)
			.then(function(res){
				res = JSON.parse(res);
				page_editor.clients = res.data; 
			})
		}

	/* ---------------------------------
	Definindo um texto para o stage de conteúdo
	--------------------------------- */
	function page_editor_text(text)
		{
			$("#adm-pages-stage").html(text);
		}

	/* ---------------------------------
	Iniciando os campos com editor HTML
	--------------------------------- */
	function page_editor_ckeditor_ini(id)
		{

			/* ---------------------------------
			Checando se a fola de estilo já existe
			--------------------------------- */
			if(typeof(CKEDITOR.stylesSet.registered.default)!='undefined')
				{
					delete CKEDITOR.stylesSet.registered.default;
				}

			/* ---------------------------------
			Iniciando a folha de estilo do cliente
			--------------------------------- */
			CKEDITOR.stylesSet.add( 'default', [

				/* Block styles */
				{ name: 'Paragraph',		element: 'p' },
				{ name: 'Heading 1',		element: 'h1' },
				{ name: 'Heading 2',		element: 'h2' },
				{ name: 'Heading 3',		element: 'h3' },
				{ name: 'Heading 4',		element: 'h4' },
				{ name: 'Heading 5',		element: 'h5' },
				{ name: 'Heading 6',		element: 'h6' },
				{ name: 'Line-height 100',	element: 'p', styles: { 'line-height':'100%' } },
				{ name: 'Line-height 120',	element: 'p', styles: { 'line-height':'120%' } },
				{ name: 'Line-height 140',	element: 'p', styles: { 'line-height':'140%' } },
				{ name: 'Line-height 160',	element: 'p', styles: { 'line-height':'160%' } },
				{ name: 'Line-height 200',	element: 'p', styles: { 'line-height':'200%' } },
				{ name: 'Line-height 240',	element: 'p', styles: { 'line-height':'240%' } },
				{ name: 'Preformatted Text',element: 'pre' },
				{ name: 'Address',			element: 'address' },
				{ name: 'Padrão',	element: 'p', styles: { 'color': 'inherit','font-style': 'inherit' } },
				{ name: 'Texto colorido',	element: 'p', styles: { 'color': '#16A98A' } },
				{ name: 'Box',	element: 'p', styles: { 'color': '#ffffff', 'padding':'20px', 'background-color':'#16A98A' } },
				
				/* Inline styles */
				{ name: 'Strong',			element: 'strong', overrides: 'b' },
				{ name: 'Emphasis',			element: 'em'	, overrides: 'i' },
				{ name: 'Underline',		element: 'u' },
				{ name: 'Strikethrough',	element: 'strike' },
				{ name: 'Subscript',		element: 'sub' },
				{ name: 'Superscript',		element: 'sup' },

				/* Object styles */
				{
					name: 'Styled Image (left)',
					element: 'img',
					attributes: { 'class': 'left' }
				},

				{
					name: 'Styled Image (right)',
					element: 'img',
					attributes: { 'class': 'right' }
				},

				{
					name: 'Compact Table',
					element: 'table',
					attributes: {
						cellpadding: '5',
						cellspacing: '0',
						border: '1',
						bordercolor: '#ccc'
					},
					styles: {
						'border-collapse': 'collapse'
					}
				},

				{ name: 'Borderless Table',		element: 'table',	styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
				{ name: 'Square Bulleted List',	element: 'ul',		styles: { 'list-style-type': 'square' } },

				/* Widget styles */

				{ name: 'Clean Image', type: 'widget', widget: 'image', attributes: { 'class': 'image-clean' } },
				{ name: 'Grayscale Image', type: 'widget', widget: 'image', attributes: { 'class': 'image-grayscale' } },

				{ name: 'Featured Snippet', type: 'widget', widget: 'codeSnippet', attributes: { 'class': 'code-featured' } },

				{ name: 'Featured Formula', type: 'widget', widget: 'mathjax', attributes: { 'class': 'math-featured' } },

				{ name: '240p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-240p' }, group: 'size' },
				{ name: '360p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-360p' }, group: 'size' },
				{ name: '480p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-480p' }, group: 'size' },
				{ name: '720p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-720p' }, group: 'size' },
				{ name: '1080p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-1080p' }, group: 'size' },

				// Adding space after the style name is an intended workaround. For now, there
				// is no option to create two styles with the same name for different widget types. See https://dev.ckeditor.com/ticket/16664.
				{ name: '240p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-240p' }, group: 'size' },
				{ name: '360p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-360p' }, group: 'size' },
				{ name: '480p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-480p' }, group: 'size' },
				{ name: '720p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-720p' }, group: 'size' },
				{ name: '1080p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-1080p' }, group: 'size' }

			] );

			CKEDITOR.replace(id,{
			toolbar : [
					['NumberedList','BulletedList','-','Outdent','Indent'],
					['Link','Unlink'],
					['Font','FontSize','Bold','Italic','Underline','Strike'],
					['TextColor','BGColor'],
					['Maximize'],
					['Table','Image'],
					['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					['Styles'],
					['Source']
					]
					});

			CKEDITOR.instances[id].on('change', function() {
				codigo = CKEDITOR.instances[id].getData();
				$("#"+id).val(codigo);
				});

		}

	/* ---------------------------------
	Editando uma página
	--------------------------------- */
	function page_editor_views_edit()
		{

			loading();

			var page_editor 	= get('../adm_pages/page_editor/views/page_editor_views_edit.html?t_='+timestamp);
			var page_content 	= get('../view/view.php?t_='+timestamp);

			Promise.all([page_editor,page_content])
				.then(function(res){

					/* ---------------------------------
					Incluíndo a página HTML
					--------------------------------- */
					$("#adm-pages-stage").html(res[0]);

					/* ---------------------------------
					Setando o conteúdo da página
					--------------------------------- */
					page_content = JSON.parse(res[1]);

					/* ---------------------------------
					Preenchendo os campos de views
					--------------------------------- */
					if(page_content.data.length>0)
						{
							$("#dv-page-editor-view-edit-list").html('');
							$.each(page_content.data,function(key,value)
								{
							  		var cod = '<div class="dv-page-editor-view-edit-list-item" data-cod="">\
							  						<img src="../'+value.thumbs+'">\
							  						<p>'+value.titulo+'</p><p>'+value.tipo+'</p></div>';
							  		$(cod).appendTo("#dv-page-editor-view-edit-list");
								});
						}

					/* ---------------------------------
					Preenchendo campos de formulários
					--------------------------------- */
					if(page_content.data.length>0)
						{
							
							var tipos_ar = array();
							

							$.each(page_content.data,function(key,value)
								{
							  		var cod = '<div class="dv-page-editor-view-edit-list-item" data-cod="">\
							  						<img src="../'+value.thumbs+'">\
							  						<p>'+value.titulo+'</p><p>'+value.tipo+'</p></div>';
							  		$(cod).appendTo("#dv-page-editor-view-edit-list");
								});
						}
					

				})
				.catch(function(err){
				    console.log('Erro no carregamento de dados', err);
				});


		}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Clique no icone de fechar upload
	--------------------------------- */
	$(document).on("click","#btn-page-editor-uploads-close",function()
		{
			page_editor_uploads_close();
		});

	/* ---------------------------------
	Removendo uma imagem
	--------------------------------- */
	$(document).on("click",".btn-page-editor-uploads-del",function()
		{
			var id_midia = $(this).attr('data-id');
			$("#dv-page-editor-uploads-confirm-remove").attr('data-id',id_midia);

			page_editor_uploads_remove_midia();
		});

	/* ---------------------------------
	Fechando o modal de confirmação de
	remoção de mídia
	--------------------------------- */
	$(document).on("click","#btn-page-editor-uploads-confirm-remove-cancel",function()
		{
			$("#dv-page-editor-uploads-confirm-remove").modal('hide');
		});

	/* ---------------------------------
	Confirmando a remoção de uma mídia
	--------------------------------- */
	$(document).on("click","#btn-page-editor-uploads-confirm-remove-confirm",function()
		{
			page_editor_uploads_remove_midia_confirm();
		});

	/* ---------------------------------
	Confirmando a seleção de uma mídia
	--------------------------------- */
	$(document).on("click",".btn-page-editor-uploads-confirm",function()
		{
			var arquivo = $(this).attr('data-arquivo');
			var url_main = $("#main").attr('data-url');
			var url_file = url_main+'/fotos/midias/'+arquivo;

			var field_type = $("#dv-page-editor-uploads").attr('data-view_type');
			var field_modo = $("#dv-page-editor-uploads").attr('data-modo');

			if(field_modo=='view')
				{
					var field_actual = $("#dv-page-editor-uploads").attr('data-view_field');
					$(".page_editor_content_field[data-view_field="+field_actual+"]").val(url_file);
					if(field_type=='img')
						{
							$(".page_editor_content_field_img[data-view_field="+field_actual+"]").css('background-image','url('+url_file+')');
						}
					if(field_type=='video')
						{
							$(".page_editor_content_field_video[data-view_field="+field_actual+"]").html('<video width="100%" height="100%" controls><source src="'+url_file+'" type="video/mp4"></video>');
						}
				}

			if(field_modo=='share')
				{
					$("#cp-page-editor-edit-share-img").val(url_file);
					$("#dv-page-editor-edit-share-img-preview").css('background-image','url('+url_file+')');
				}

			page_editor_uploads_close();

		});

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Exibindo o modal de remoção de uma mídia
	--------------------------------- */
	function page_editor_uploads_remove_midia()
		{
			$("#dv-page-editor-uploads-confirm-remove").modal('show');
		}

	/* ---------------------------------
	Confirmando a remoção de uma mídia
	--------------------------------- */
	function page_editor_uploads_remove_midia_confirm()
		{

			id_midia = $("#dv-page-editor-uploads-confirm-remove").attr('data-id');
			$.getJSON('../adm_pages/page_editor/data/midias_remove.php?id='+id_midia,function(response)
				{
					msg = new message();
					if(response.status=='1')
						{
							msg.add('<i class="icon check green"></i> arquivo removido da biblioteca');
						} else {
							msg.add('<i class="icon ban red"></i> '+response.message);
						}

					$("#dv-page-editor-uploads-confirm-remove").modal('hide');
					page_editor_uploads_load_list();

				})

		}

	/* ---------------------------------
	Iniciando o sistema de upload de mídias
	--------------------------------- */
	function page_editor_uploads_init(config_upload_ini)
		{

			$("#dv-page-editor-overlay").html('');

			var url_html = get('../adm_pages/page_editor/views/page_editor_midias.html');
			Promise.all([url_html])
				.then(function(res){

					$("#dv-page-editor-overlay").html(res[0]);

					$("#dv-page-editor-uploads").attr('data-view_field',config_upload_ini.field);
					$("#dv-page-editor-uploads").attr('data-view_type',config_upload_ini.type);
					$("#dv-page-editor-uploads").attr('data-modo',config_upload_ini.modo);

					$("#dv-page-editor-overlay").css('display','block');

					page_editor_uploads_load_list();
					$("#dv-page-editor-uploads").removeClass('hide');

					var config_upload = [];
					config_upload['alvo']         = '#dv-page-editor-upload';
					config_upload['funcao']       = page_editor_uploads_save;
					config_upload['multi']        = false;
					config_upload['extensions']   = ['mp4','jpg', 'jpeg', 'png', 'gif'];
					config_upload['btn_text']     = 'Enviar foto';
					config_upload['btn_color']    = 'blue';
					config_upload['btn_size']     = 'small';
					config_upload['btn_icon']     = 'photo';
					config_upload['dir_upload']   = 'components/upload_html5/';

					upload_html5(config_upload);

				})
				.catch(function(err){
				    console.log('Erro no carregamento de dados', err); 
				});
		}

	/* ---------------------------------
	Efetuando upload de uma mídia
	--------------------------------- */
	function page_editor_uploads_save(res)
		{

			var form = new FormData();
			form.append("cod", res.cod);
			form.append("nome", res.nome);
			form.append("mime", res.mime);

			var url = '../adm_pages/page_editor/data/midias_upload.php';
			var settings = {
			  "async": true,
			  "mode": "no-cors",
			  "url": url,
			  "method": "POST",
			  "processData": false,
			  "contentType": false,
			  "mimeType": "multipart/form-data",
			  "data": form
			}

			$.ajax(settings).done(function (response)
				{

					var res = JSON.parse(response);
					console.log('Upload Save Res: ',res);
			 		msg = new message();
					if(res.status=="1")
						{
							msg.add('<i class="icon check green"></i> Arquivo enviado com sucesso!');
						} else {
							msg.add('<i class="icon ban red"></i> '+res.message);
						}
					page_editor_uploads_load_list();
				});

		}

	/* ---------------------------------
	Listando mídias
	--------------------------------- */
	function page_editor_uploads_load_list()
		{

			var file_type = $("#dv-page-editor-uploads").attr('data-view_type');
			let url_main = $("#main").attr('data-url');

			$("#dv-page-editor-uploads-list-stage").html('');
			$.getJSON('../adm_pages/page_editor/data/midias_list.php?type='+file_type,function(response)
				{
					console.log('Res imagens: ',response);

					$.each(response.data,function(indice,valor)
						{

							id = valor.id;
							arquivo = valor.arquivo;
							tipo = valor.tipo;
							let url_file = url_main+'/fotos/midias/'+arquivo;

							if(tipo=='image')
								{
									content = '<div class="dv-page-editor-uploads-list-item aspect-square bg-grey-ultralight bg-auto shadow" style="background-image: url('+url_file+')">\
						                            <div></div>\
						                        </div>';
								}

							if(tipo=='video')
								{
									content = '<div class="dv-page-editor-uploads-list-item aspect-square bg-grey-ultralight bg-auto shadow">\
													<div>\
														<div class="aspect-wide pos-middle-v">\
															<div>\
																<video width="100%" height="100%" controls>\
																  <source src="'+url_file+'" type="video/mp4">\
																</video>\
															</div>\
														</div>\
													</div>\
												</div>';
								}

							var cod = '<div class="grid-3" data-id_midia="'+id+'">\
						                    <div class="pad-20">\
						                    	<div class="bg-white pad-b-10"><input type="text" value="'+url_file+'" class="cp_midias_url"></div>\
						                        '+content+'\
						                        <div class="pos-b-r">\
						                            <div class="pad-30">\
						                                <div class="btn-page-editor-uploads-confirm labeled ui icon button green mini" data-id="'+id+'" data-arquivo="'+arquivo+'">\
						                                    <i class="check icon"></i>\
						                                    Selecionar\
						                                </div>\
						                                <div class="btn-page-editor-uploads-del ui icon button red mini" data-id="'+id+'">\
						                                    <i class="trash icon"></i>\
						                                </div>\
						                            </div>\
						                        </div>\
						                    </div>\
						                </div>';

						    $(cod).appendTo("#dv-page-editor-uploads-list-stage");

						});
				})

		}

	/* ---------------------------------
	Fechando o upload de mídias
	--------------------------------- */
	function page_editor_uploads_close()
		{
			$("#dv-page-editor-uploads").addClass('hide');
			$("#dv-page-editor-overlay").html('');
			$("#dv-page-editor-overlay").css('display','none');
		}
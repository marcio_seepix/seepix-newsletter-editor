/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Abrindo o editor de views
	--------------------------------- */
	$(document).on("click","#btn-page-editor-edit-views",function()
		{
			page_editor_views_edit();
		});	

	/* ---------------------------------
	Voltando para a p�gina inicial ao
	clicar no voltar no editor de views
	--------------------------------- */
	$(document).on("click","#btn-page-editor-view-edit-back",function()
		{
			page_editor_ini();
		});	

	/* ---------------------------------
	Adicionando um campo edit�vel
	--------------------------------- */
	$(document).on("click","#btn-page-editor-view-edit-field-add",function()
		{
			page_editor_views_edit_field_add();	
		});	

	/* ---------------------------------
	Removendo um campo edit�vel
	--------------------------------- */
	$(document).on("click",".btn-page-editor-view-edit-field-remove",function()
		{

			$(this).parent().parent().remove();
		});	

	/* ---------------------------------
	Salvando a view
	--------------------------------- */
	$(document).on("click","#btn-page-editor-view-edit-save",function()
		{
			page_editor_views_edit_save();	
		});

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Iniciando o editor de views 
	--------------------------------- */
	function page_editor_views_edit()
		{

			page_editor_loading();

			var timestamp = new Date().getTime();

			var page_editor 	= get('../adm_pages/page_editor/views/page_editor_views_edit.html?t_='+timestamp);
			var page_content 	= get('../view/view.php?t_='+timestamp);

			Promise.all([page_editor,page_content])
				.then(function(res){

					/* ---------------------------------
					Inclu�ndo a p�gina HTML
					--------------------------------- */
					$("#adm-pages-stage").html(res[0]);

					/* ---------------------------------
					Setando o conte�do da p�gina
					--------------------------------- */
					page_content = JSON.parse(res[1]);

					/* ---------------------------------
					Preenchendo os campos de views
					--------------------------------- */
					if(page_content.data.length>0)
						{
							$("#dv-page-editor-view-edit-list").html('');
							$.each(page_content.data,function(key,value)
								{
							  		var cod = '<div class="dv-page-editor-view-edit-list-item" data-cod="">\
							  						<img src="../'+value.thumbs+'">\
							  						<p>'+value.titulo+'</p><p>'+value.tipo+'</p></div>';
							  		$(cod).appendTo("#dv-page-editor-view-edit-list");
								});
						}

					/* ---------------------------------
					Preenchendo campos de formul�rios
					--------------------------------- */
					if(page_content.data.length>0)
						{
							
							var tipos_ar = [];
							

							$.each(page_content.data,function(key,value)
								{
							  		var cod = '<div class="dv-page-editor-view-edit-list-item" data-cod="">\
							  						<img src="../'+value.thumbs+'">\
							  						<p>'+value.titulo+'</p><p>'+value.tipo+'</p></div>';
							  		$(cod).appendTo("#dv-page-editor-view-edit-list");
								});
						}
					
					page_editor_views_edit_ini();

				})
				.catch(function(err){
				    console.log('Erro no carregamento de dados', err);
				});


		}

	/* ---------------------------------
	Iniciando o formul�rio e ativando o 
	upload de imagem e a formata��o de campos
	--------------------------------- */
	function page_editor_views_edit_ini()
		{

			format.diretorio('#cp-page-editor-view-edit-field-cod');

			var config_upload = [];
			config_upload['alvo']         = '#dv-page-editor-view-edit-upload-img';
			config_upload['funcao']       = page_editor_views_edit_upload_end;
			config_upload['multi']        = false;
			config_upload['extensions']   = ['png'];
			config_upload['btn_text']     = 'Enviar imagem';
			config_upload['btn_color']    = 'orange';
			config_upload['btn_size']     = 'small';
			config_upload['btn_icon']     = 'photo';
			config_upload['dir_upload']   = 'components/upload_html5/';

			upload_html5(config_upload);
		}

	/* ---------------------------------
	Fun��o executada ao final do upload
	para atualiza��o do preview da img
	--------------------------------- */
	function page_editor_views_edit_upload_end(res)
		{
			if(res.success==true)
				{
					$("#cp-page-editor-view-edit-img").val(res.cod);
					$("#dv-page-editor-view-edit-upload-img-preview").html('<img src="components/upload_html5/upload_files/'+res.cod+'" class="pos-middle">');
				}
		}

	/* ---------------------------------
	Adicionando um campo
	--------------------------------- */
	function page_editor_views_edit_field_add()
		{
			var cod = $("#cp-page-editor-view-edit-field-cod").val();
			var label = $("#cp-page-editor-view-edit-field-label").val();
			var type = $("#cp-page-editor-view-edit-field-type").val();
			var order = $("#cp-page-editor-view-edit-field-order").val();

			if(cod=='')
				{
					msg = new message();
					msg.add('<i class="icon check green"></i> Informe o c�digo do seu campo edit�vel');
					$("#cp-page-editor-view-edit-field-cod").focus();
					return false;
				}

			if(label=='')
				{
					msg = new message();
					msg.add('<i class="icon check green"></i> Informe o c�digo do seu campo edit�vel');
					$("#cp-page-editor-view-edit-field-label").focus();
					return false;
				}

			if(type=='-1')
				{
					msg = new message();
					msg.add('<i class="icon check green"></i> Selecione o tipo de input de dados');
					$("#cp-page-editor-view-edit-field-label").focus();
					return false;
				}

			if(order=='' || order==null || order<0)
				{
					order = 0;
				}

			var cod_table = '<tr class="tr-page-editor-view-edit-field">\
								<td>'+cod+'</td>\
								<td>'+label+'</td>\
								<td>'+type+'</td>\
								<td>'+order+'</td>\
								<td>\
									<div class="btn-page-editor-view-edit-field-remove ui icon button small red basic">\
									  <i class="trash icon"></i>\
									</div>\
								</td>\
							</tr>';

			$(cod_table).appendTo("#dv-page-editor-view-edit-field-list");

		}

	/* ---------------------------------
	Salvando a view
	--------------------------------- */
	function page_editor_views_edit_save()
		{

			var titulo = $("#cp-page-editor-view-edit-titulo").val();
			var description = $("#cp-page-editor-view-edit-description").val();
			var cod_html = $("#cp-page-editor-view-edit-cod").val();
			var img = $("#cp-page-editor-view-edit-img").val();
			var fields = "";

			if(titulo=='')
				{
					msg = new message();
					msg.add('<i class="icon red ban"></i> Informe o t�tulo de sua views.');
					$("#cp-page-editor-view-edit-titulo").focus();
					return false;
				}

			if(description=='')
				{
					msg = new message();
					msg.add('<i class="icon red ban"></i> Descreva sua views.');
					$("#cp-page-editor-view-edit-description").focus();
					return false;
				}

			if(cod_html=='')
				{
					msg = new message();
					msg.add('<i class="icon red ban"></i> Informe o c�digo HTML de sua view.');
					$("#cp-page-editor-view-edit-cod").focus();
					return false;
				}

			if(img=='')
				{
					msg = new message();
					msg.add('<i class="icon red ban"></i> Envie a imagem de thumbs de sua view.');
					return false;
				}

			/* ---------------------------------
			Coletando os fields
			--------------------------------- */
			if($(".tr-page-editor-view-edit-field").length==0)
				{
					msg = new message();
					msg.add('<i class="icon red ban"></i> Configure os campos edit�veis de sua view.');
					return false;	
				}

			loading();

			fields_ar = [];
			for(var i = 0; i < $(".tr-page-editor-view-edit-field").length; i++)
				{
				  	var field_cod = $(".tr-page-editor-view-edit-field:eq("+i+") > td:eq(0)").text();
				  	var field_label = $(".tr-page-editor-view-edit-field:eq("+i+") > td:eq(1)").text();
				  	var field_type = $(".tr-page-editor-view-edit-field:eq("+i+") > td:eq(2)").text();
				  	var field_order = $(".tr-page-editor-view-edit-field:eq("+i+") > td:eq(3)").text();

				  	var field_line = field_cod+'_|_'+field_label+'_|_'+field_type+'_|_'+field_order;
				  	fields_ar.push(field_line);
				};

			fields = fields_ar.join(',');

			var form = new FormData();
			form.append("data[titulo]", titulo);
			form.append("data[description]", description);
			form.append("data[cod_html]", cod_html);
			form.append("data[img]", img);
			form.append("data[fields]", fields);
			
			var url = '../adm_pages/page_editor/data/view_insert.php';
			var settings = {
			  "async": true,
			  "crossDomain": true,
			  "crossOrigin": 'anonymous',
			  "url": url,
			  "method": "POST",
			  "processData": false,
			  "contentType": false,
			  "mimeType": "multipart/form-data",
			  "data": form
			}
			
			$.ajax(settings).done(function (response) 
				{
			
					var retorno_json = JSON.parse(response);
			
			  		msg = new message();
					if(retorno_json.status=="1")
						{
							
							msg.add('<i class="icon check green"></i> View adicionada com sucesso!');
							setTimeout(function(){
								page_editor_views_edit();
								}, 2000);
			
						} else {
			
							msg.add('<i class="icon ban red"></i> '+retorno_json.message);
							setTimeout(function(){
								loading_out();
								}, 2000);
						}
				});

		}
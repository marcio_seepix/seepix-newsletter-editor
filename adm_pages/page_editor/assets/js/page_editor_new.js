/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/* ---------------------------------
	Confirmando o cadastro de nova página
	--------------------------------- */
	$(document).on("click","#btn-page-editor-new-save",function()
		{
			page_editor_new_save();
		});

	/* ---------------------------------
	Cancelando o formulário de cadastro
	--------------------------------- */
	$(document).on("click","#btn-page-editor-new-cancel",function()
		{
			$('#dv-page-editor-new-form').modal('hide');
		});

	/* ---------------------------------
	Inserir a página no BD
	--------------------------------- */
	function page_editor_new_save()
		{

			/* ---------------------------------
			Buscando os dados
			--------------------------------- */
			var title = $("#cp-page-editor-new-title").val();
			var client = $("#cp-page-editor-new-client").val();
			var tipo = $("#cp-page-editor-new-tipo-page").val();

			/* ---------------------------------
			Verificando campos vazios
			--------------------------------- */
			if(title=='' || client=='-1' || tipo=='-1')
				{

					$(".modal-page-editor-form-new-error").html('<i class="icon red ban"></i> <span class="red">Todos os campos são obrigatórios</span>');
					$("#cp-page-editor-new-title").parent().addClass('error');

					setTimeout(function(){
						$(".modal-page-editor-form-new-error").html('');
						}, 5000);

					return false;

				} else {
					$("#cp-page-editor-new-title").parent().removeClass('error');
				}

			var data_post = new FormData();
			data_post.append("title",title);
			data_post.append("cliente",client);
			data_post.append("tipo",tipo);

			var url = '../adm_pages/page_editor/data/new_insert.php';
			fetch(url,{
					method: 'POST',
					mode: 'no-cors',
					credentials: "same-origin",
					body:data_post
				})
				.then(function(response){

					if(response.ok)
						{
							return response.json();
						} else{
							msg = new message();
							msg.duration(5);
							msg.add('<i class="icon ban red"></i> Ocorreu um erro durante o carregamento desta página');
						}

				})
				.then(function(data){

					if(data.status=='1')
						{

							msg = new message();
							msg.duration(5);
							msg.add('<i class="icon check green"></i> '+data.message);

							$('#dv-page-editor-new-form').modal('hide');

							id = data.data[0].id;
							page_editor_edit(id);

						} else{

							msg = new message();
							msg.duration(5);
							msg.add('<i class="icon ban red"></i> '+data.message);

							$('#dv-page-editor-new-form').modal('hide');

						}

					$("#cp-page-editor-new-title").val('');
					$("#cp-page-editor-new-client").val('-1');
					$("#cp-page-editor-new-tipo-page").val('-1');

				})
				.catch(function(err){
					console.log(err);
				})


		}
# CONSTRUTOR DE NEWSLETTER
Sistema para facilitar a cria��o de newsletters recorrentes

## Views (Blocos em HTML edit�veis)
### Arquivo HTML
O arquivo HTML da view deve ser salvo no diret�rio `` /view/(cod_da_view)`` com o nome ``view.html`` e uma imagem de thumbnail ``thumb.png`` no tamanho ``240x240px``

```
/view/(nome_da_view)/view.html
/view/(nome_da_view)/thumb.png
```
### Configurar view no BD
A view deve ser configurada no banco de dados, na tabela ``view``

- **id_cliente** - Utilize o ID de um cliente para configurar uma view exclusiva.
- **id_tipo** - Informe o ID do tipo de conte�do em edi��o (News, Landing Page, Websites, etc). Verificar os tipos dispon�veis na tabela ``page_tipo``
- **cod** - � obrigat�rio que seja id�ntico ao diret�rio da view
- **titulo** - T�tulo da view
- **description** - Descri��o da view
- **ativo** - Ativa ou bloqueia o uso de uma view

Cada campo edit�vel da view deve ser configurado no BD da tabela ``view_fields``
- **id_view** - ID da view
- **cod** - C�digo exclusivo de cada campo edit�vel da view
- **label** - R�tulo que ser� exibido ao usu�rio no momento da edi��o da view
- **type** - Tipo de conte�do edit�vel (ver tabela abaixo)
- **ordem** - Ordem de exibi��o do campo no formul�rio de edi��o

## Tipos de campos edit�veis
- color
- hiperlink
- img
- longtext
- number
- text
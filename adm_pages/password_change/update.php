<?php
header("Content-type: application/json; charset=iso-8859-1");

include_once('../../config/config.php');
include_once('../../class/db.class.php');
include_once('../../php/functions.php');

$content['status'] = 0;
$content['message'] = '';
$content['data'] = '';
$status = 0;

$msg_retorno[1] = 'Senha atualizada com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro desconhecido durante o update de dados.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Erro no recebimento de dados.';
$msg_retorno[-3] = 'Ocorreu um erro durante a atualiza��o no Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}


if($status==0)
	{
		if(!isset($_POST['pwd']))
			{
				$status = -2;
			}
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		$user_id = (integer)$_COOKIE['userId'];
		$pwd = get($_POST['pwd']);
		$pwd = trim($pwd);
		
		if($status==0)
			{
				if($pwd!='')
					{
						$pwd = md5(PWD_HASH.$pwd);

						$db = new db;
						$db->query_exec("Update spx_adm set senha='$pwd' where id='$user_id'");

						$status = 1;

					}
			}
	}

$content['status'] 	= $status;
$content['message'] = utf8_encode($msg_retorno[$status]);
$content['data'] 	= '';

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($content);
?>

<script>
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
$(document).on("click","#btn-password-change-save",function()
	{
		password_change_confirm();
	});	

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
function password_change_confirm()
	{

		var pwd = $("#cp-password-change-new").val();
		var pwd_confirm = $("#cp-password-change-new-confirm").val();

		if(pwd=='')
			{
				$("#cp-password-change-new").focus();
				msg = new message();
				msg.add('<i class="icon red ban"></i> Preencha corretamente sua nova senha');	
				return false;
			}

		if(pwd_confirm=='')
			{
				$("#cp-password-change-new-confirm").focus();
				msg = new message();
				msg.add('<i class="icon red ban"></i> Preencha corretamente sua nova senha');	
				return false;
			}

		if(pwd.length<6)
			{
				$("#cp-password-change-new").focus();
				msg = new message();
				msg.add('<i class="icon red ban"></i> Sua senha deve ter ao menos 6 caracteres');
				return false;	
			}

		if(pwd!=pwd_confirm)
			{
				$("#cp-password-change-new").focus();
				msg = new message();
				msg.add('<i class="icon red ban"></i> Suas senhas n�o combinam');
				return false;	
			}

		var url = 'pages/password_change/update.php';
		var data = 'pwd='+pwd;
		
		loading();
		$.post(url,data,function(response){
		
				msg = new message();
				if(response.status==1)
					{
						msg.add('<i class="icon check green"></i> '+response.message);
					} else {
						msg.add('<i class="icon red ban"></i> '+response.message);
					}
		
				loading_out();
		
			},"json");

	}

</script>
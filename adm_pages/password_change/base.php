<div class="pos-full">
    <div class="pad-30 bg-white rounded shadow">
        <p class="font-size-6"> Alterar senha </p>
        <p> Altere sua senha de acesso � intranet. Ele deve ter ao menos <strong>6 caracteres.</strong> </p>
        <div class="ui form">
            <div class="fields three">
                <div class="field">
                    <label>Nova senha:</label>
                    <input id="cp-password-change-new" type="password">
                </div>
                <div class="field">
                    <label>Repita sua nova senha:</label>
                    <input id="cp-password-change-new-confirm" type="password">
                </div>
                <div class="field">
                    <label>&nbsp;</label>
                    <button id="btn-password-change-save" class="ui primary button">
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
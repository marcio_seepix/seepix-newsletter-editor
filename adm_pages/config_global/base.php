<div id="dv-contato">
	<div class="bg-white shadow pad-20">
		<p class="font-size-6 bold">Configura��es diversas</p>
		<p class="font-size-3">Gerencie conte�dos diversos no sistema.</p>

		<div id="dv-contato-stage" class="margin-t-10 bg-grey-ultralight pad-20">
			<div class="ui top attached tabular menu">
				<a class="item active" data-tab="links_externos">Links Externos e textos</a>
				<a class="item" data-tab="email">Configura��es de email</a>
				<a class="item" data-tab="redes_sociais">Redes Sociais</a>
			</div>
			<div class="ui bottom attached tab segment active" data-tab="links_externos">
				
				<div class="form ui">
					<div class="field">
						<label>Hor�rio de atendimento</label>
						<input type="text" class="cp-global-config-field" data-cod="horario_atendimento">
					</div>
					<div class="field">
						<label>Telefones</label>
						<textarea class="cp-global-config-field" data-cod="telefones"></textarea>
					</div>
					<div class="field">
						<label>Endere�o</label>
						<textarea class="cp-global-config-field" data-cod="endereco"></textarea>
					</div>
					<div class="field">
						<label>Localiza��o</label>
						<textarea class="cp-global-config-field" data-cod="localizacao"></textarea>
					</div>
				</div>

			</div>
			<div class="ui bottom attached tab segment" data-tab="email">
				<div class="form ui">
					<div class="field">
						<label>Email de envio de mensagens</label>
						<input type="text" class="cp-global-config-field" data-cod="email_saida">
					</div>
					<div class="field">
						<label>SMTP:</label>
						<input type="text" class="cp-global-config-field" data-cod="email_smtp">
					</div>
					<div class="field">
						<label>Porta de sa�da (Padr�o 587):</label>
						<input type="text" class="cp-global-config-field" data-cod="email_porta">
					</div>
					<div class="field">
						<label>Senha do email:</label>
						<input type="text" class="cp-global-config-field" data-cod="email_senha">
					</div>
					<div class="field">
						<label>Criptografia(TLS):</label>
						<select class="cp-global-config-field" data-cod="email_config_tls">
							<option value="1">Ativa</option>
							<option value="0">Inativa</option>
						</select>
					</div>
					<div class="field">
						<label>Email autenticado:</label>
						<select class="cp-global-config-field" data-cod="email_config_autenticado">
							<option value="1">Sim</option>
							<option value="0">N�o</option>
						</select>
					</div>
					<hr></hr>
					<div class="field">
						<label>Email de recebimento de mensagens do site:</label>
						<input type="text" class="cp-global-config-field" data-cod="email_recebimento">
					</div>
				</div>
			</div>
			<div class="ui bottom attached tab segment" data-tab="redes_sociais">

				<div class="pad-20 bg-grey-ultralight margin-b-10 rounded">
					<p class="font-size-2">Informe as URL�s dos canais de redes sociais.</p>
				</div>

				<div class="form ui">
					<div class="field">
						<label>Facebook</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_facebook">
					</div>
					<div class="field">
						<label>Instagram</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_instagram">
					</div>
					<div class="field">
						<label>Whastapp</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_whatsapp">
					</div>
					<div class="field">
						<label>LinkedIn</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_linkedin">
					</div>
					<div class="field">
						<label>Twitter</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_twitter">
					</div>
					<div class="field">
						<label>YouTube</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_youtube">
					</div>
					<div class="field">
						<label>Google Plus</label>
						<input type="text" class="cp-global-config-field" data-cod="redes_sociais_google_plus">
					</div>
				</div>

			</div>
			
			<div class="margin-t-20 text-r">
				<div  id="btn-config-globa-save" class="ui labeled icon button blue">
					<i class="save icon"></i>
					Salvar
				</div>
			</div>
		</div>
	</div>
</div>

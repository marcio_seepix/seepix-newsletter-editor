<?php
header("Content-type: application/json; charset=UTF-8");

include_once('../../config/config.php');
include_once('../../class/db.class.php');
include_once('../../php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados salvos com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualiza��o de conte�do.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informa��es.';
$msg_retorno[-3] = 'Ocorreu um erro durante a inser��o no Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		if(isset($_POST['data']))
			{

				$db = new db;

				$content = $_POST['data'];
				$id_language = (integer)$content['id_language'];

				foreach($content['field'] as $key => $value) 
					{

						if(!mb_detect_encoding($value, 'UTF-8', true))
							{
								$value = utf8_encode($value);
							}
						
						/* ---------------------------------
						Contanto os registros
						--------------------------------- */
						$check = $db->num_rows("Select * from config_global where id_language='$id_language' and cod='$key'");
						if($check>0)
							{
								$query = "Update config_global set valor='$value' where id_language='$id_language' and cod='$key'";
							} else {
								$query = "Insert into config_global (id_language,cod,valor) VALUES ('$id_language','$key','$value')";
							}

						$db->query_exec($query);

					}

				$status = 1;

			} else {
				$status = -2;
			}

	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

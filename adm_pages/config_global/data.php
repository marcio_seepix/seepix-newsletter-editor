<?php
//exibe
error_reporting(E_ALL);
ini_set('display_errors', 1);

header("Content-type: application/json; charset=iso-8859-1");

include_once('../../config/config.php');
include_once('../../class/db.class.php');
include_once('../../php/functions.php');

$response['status'] = 0;
$response['message'] = '';
$response['data'] = '';
$status = 0;

$msg_retorno[1] = 'Dados carregados com sucesso!';
$msg_retorno[0] = 'Ocorreu um erro durante a atualiza��o de conte�do.';
$msg_retorno[-1] = 'Voc� n�o est� conectado.';
$msg_retorno[-2] = 'Ocorreu um erro no recebimento de informa��es.';
$msg_retorno[-3] = 'Ocorreu um erro durante a inser��o no Banco de Dados.';

/* ---------------------------------
Usu�rio desconectado
--------------------------------- */
if(!adm_logged())
	{
		$status = -1;
	}

/* ---------------------------------
Usu�rio logado
--------------------------------- */
if(adm_logged())
	{

		$db = new db;
		$dados = $db->load("Select id,cod,valor from config_global");
		$response['data'] = $dados;

		$status = 1;

	}

$response['status'] 	= $status;
$response['message'] = $msg_retorno[$status];

header('Content-type: application/json');
header('Charset: utf-8');
echo json_encode($response);
?>

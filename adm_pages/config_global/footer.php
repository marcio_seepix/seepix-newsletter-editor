<script>

$(function(){
		$('.menu .item').tab();
		config_global_load_content();
	})

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ACTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).on("click","#btn-config-globa-save",function()
	{
		config_global_save();
	});	

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FUN��ES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* ---------------------------------
Salvando os registros
--------------------------------- */
function config_global_save()
	{

		var form = new FormData();
		$.each($(".cp-global-config-field"), function( key, value)
			{
				var cod = $(this).attr('data-cod');
				var val = $(this).val();
		  		
		  		form.append("data[field]["+cod+"]", val);
			});
		
		var url = 'pages/config_global/save.php';
		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "crossOrigin": 'anonymous',
		  "no-cors":false,
		  "url": url,
		  "method": "POST",
		  "processData": false,
		  "contentType": false,
		  "mimeType": "multipart/form-data",
		  "data": form
		}
		
		$.ajax(settings).done(function (response) 
			{
				
				res = JSON.parse(response);

				msg = new message();
				if(res.status==1)
					{
						msg.add('<i class="icon check green"></i> '+res.message);
					} else {
						msg.add('<i class="icon ban red"></i> '+res.message);
					}
				
			});

	}

/* ---------------------------------
Load de conte�do da p�gina
--------------------------------- */
function config_global_load_content()
	{
		$(".cp-global-config-field").val("");
		$.getJSON('pages/config_global/data.php',function(lista)
			{

				$.each(lista.data,function(indice,valor)
					{
						$(".cp-global-config-field[data-cod="+valor.cod+"]").val(valor.valor);
					});

			})

	}

/* ---------------------------------
Iniciando os campos com editor HTML
--------------------------------- */
function ckeditor_ini(id)
	{
		
		CKEDITOR.replace(id,{
		toolbar : [
				['NumberedList','BulletedList','-','Outdent','Indent'],
				['Link','Unlink'],
				['Bold','Italic','Underline','Strike'],
				['TextColor','BGColor'],
				['Image','Table','Format'],
				['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
				['Source'],
				]
				});

		CKEDITOR.instances[id].on('change', function() { 
			codigo = CKEDITOR.instances[id].getData();
			$("#"+id).val(codigo);
			});
		
	}

/* ---------------------------------
Encerrando os campos do Ckeditor
--------------------------------- */
function ckeditor_end(id)
	{
		if(CKEDITOR.instances[id])
			{
				CKEDITOR.instances[id].destroy();
			}
	}
</script>
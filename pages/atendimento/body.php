<div class="pad-20">
	<p class="titulo-1 color pad-t-10">Atendimento</p>
	<?php 
	$config_global['endereco'] = str_replace("\n", "<br>", $config_global['endereco']);
	print '<div class="pad-10">';
		print '<p class="bold margin-0">ENDERE�O</p>';
		print '<p>'.$config_global['endereco'].'</p>'; 
	print '</div>';
	print '<div class="pad-10">';
		print '<p class="bold margin-0">HOR�RIO DE ATENDIMENTO</p>';
		print '<p>'.$config_global['horario_atendimento'].'</p>';
	print '</div>';
	print '<div class="pad-10">';
		print '<p class=" bold margin-0">FALE CONOSCO</p>';
	    
	    $enderecos = $config_global['telefones']; 
	    $enderecos = str_replace("\n", "</p><p class=\"desktop-font-size-4 tablet-font-size-4 phone-font-size-3 margin-0 \">", $enderecos);
	    print '<p class="desktop-font-size-4 tablet-font-size-4 phone-font-size-3 margin-0">'.$enderecos.'</p>';
	print '</div>';
	print '<div class="pad-10">';
		print '<p class="bold margin-0">WHATSAPP</p>';
		print '<p>'.$config_global['redes_sociais_whatsapp'].'</p>';    		               
	print '</div>';
	?>
</div>
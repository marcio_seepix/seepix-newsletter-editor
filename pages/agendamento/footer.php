<link rel="stylesheet" type="text/css" href="components/message/css/message.css">
<script type="text/javascript" src="assets/js/functions/createToken.js"></script>
<script type="text/javascript" src="components/message/js/message.min.js"></script>
<script>

var campos 				= ['nome','nasc_dia','nasc_mes','nasc_ano','sexo','email','endereco','num','complemento','bairro','cep','cidade','uf','telefone','tel_complemento','celular','modalidade','id_medico','observacao','tipo_atendimento','convenio_nome','convenio_plano','data_agendamento','periodo_agendamento'];
var campos_obrigatorios = ['nome','nasc_dia','nasc_mes','nasc_ano','sexo','email','telefone','modalidade','tipo_atendimento','data_agendamento'];
var total_campos 		= campos.length;

for(var i=0; i<total_campos;i++)
	{
		var campo = campos[i];
		valor = localStorage.getItem("agendamento_"+campo);

		if(valor!=null){
			// $("#cp-"+campo).val(valor);
			}
		
	}

$(function(){
	
		$(document).on("click","#btn_agendar",function()
			{
				agendar();	
			});	
	})

function agendar(){

	var form = new FormData();

	var itens = [];
	for(var i=0; i<total_campos;i++){
		
		var atual = campos[i];
		var valor = $("#cp-"+atual).val();

		// localStorage.setItem("agendamento_"+atual,valor);

		if(valor=='' || valor=='-1'){
			var confere = $.inArray(atual, campos_obrigatorios);
			if(confere>-1){
				
				msg = new message();
				msg.add('<i class="icon red ban"></i> Preencha corretamente o campo selecionado');

				$("#cp-"+atual).parent().addClass('error');
				$("#cp-"+atual).focus();
				return false;
				} 
			} else {
				$("#cp-"+atual).parent().removeClass('error');	
			}
		
		
		form.append("data["+atual+"]", valor);
		
		}

	// if(confirm('Tem certeza que deseja enviar este agendamento agora?'))
	// 	{
		
			var url = 'pages/agendamento/agenda_insere.php';
			var settings = {
					"async": true,
					"crossDomain": true,
					"crossOrigin": 'anonymous',
					"url": url,
					"method": "POST",
					"processData": false,
					"contentType": false,
					"mimeType": "multipart/form-data",
					"data": form
					}

			$.ajax(settings).done(function (response) 
				{

					var retorno_json = JSON.parse(response);

			  		msg = new message();
					if(retorno_json.status=="1")
						{
							msg.add('<i class="icon check green"></i> Agendamento enviado com sucesso!');

							$("#dv-agenda").addClass('hide');
							$("#dv-agenda-confirm").removeClass('hide');

						} else {
							msg.add('<i class="icon ban red"></i> '+retorno_json.message);
						}
				});
		// }
	}
</script>
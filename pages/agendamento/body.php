<p class="titulo-1 color pad-t-10">Pr�-agendamento de consultas e Exames</p>
<?php
foreach ($dados_texto as $key => $value)
	{

		$value['texto'] = str_replace("\n", "</p><p>", $value['texto']);

		print '<div class="agenda-textos">';
			print '<h4>'.$value['titulo'].'</h4>';
			print '<p>'.$value['texto'].'</p>';
		print '</div>';
	
	}
?>

<div id="dv-agenda-confirm" class="ui icon message hide">
  <i class="check green icon"></i>
  <div class="content">
    <div class="header">
      Solicita��o enviada
    </div>
    <p>Sua solicita��o de agendamento de consulta foi enviada. Por favor aguarde nossa confirma��o de data e hor�rio.</p>
  </div>
</div>

<div id="dv-agenda" class="ui form segment">
	<h4 class="ui dividing header">Dados pessoais</h4>
	<div class="field wide">
	    <label>Nome</label>
	    <input id="cp-nome" type="text" placeholder="">
	</div>
	<div class="field">
		<div class="two fields">
			<div class="field six wide">
				<label>Nascimento:</label>
				<div class="three fields">
					<div class="field five wide">
						<select id="cp-nasc_dia">
							<option value="-1">-- Dia --</option>
							<?php
							for ($i=1; $i < 32; $i++)
								{ 
									print '<option value="'.$i.'">'.$i.'</option>';
								}
							?>
						</select>
					</div>
					<div class="field five wide">
						<select id="cp-nasc_mes">
							<option value="-1">-- M�s --</option>
							<?php
							$meses = array('','Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
							for ($i=1; $i < 13; $i++)
								{ 
									print '<option value="'.$i.'">'.$meses[$i].'</option>';
								}
							?>
						</select>
					</div>
					<div class="field five wide">
						<select id="cp-nasc_ano">
							<option value="-1">-- Ano --</option>
							<?php
							$ano_atual = date('Y');
							for ($i=$ano_atual; $i > ($ano_atual - 110); $i--)
								{ 
									print '<option value="'.$i.'">'.$i.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="field six wide">
				<label>Sexo:</label>
			    <select id="cp-sexo">
			    	<option value="-1">- selecione -</option>
			    	<option value="f">Feminino</option>
			    	<option value="m">Masculino</option>
			    </select>
			</div>
		</div>
	</div>
	<h4 class="ui dividing header">Dados de contato</h4>
	<div class="field wide">
	    <label>Email</label>
	    <input id="cp-email" type="text" placeholder="">
	</div>
	<div class="field wide">
	    <div class="three fields">
			<div class="field nine wide">
				<label>Endere�o:</label>
			    <input id="cp-endereco" type="text">
			</div>
			<div class="field three wide">
				<label>N�mero:</label>
			    <input id="cp-num" type="text">
			</div>
			<div class="field four wide">
				<label>Complemento:</label>
			    <input id="cp-complemento" type="text">
			</div>
		</div>
		<div class="two fields">
			<div class="field nine wide">
				<label>Bairro:</label>
			    <input id="cp-bairro" type="text">
			</div>
			<div class="field four wide">
				<label>CEP:</label>
			    <input id="cp-cep" type="text">
			</div>
		</div>
		<div class="two fields">
			<div class="field nine wide">
				<label>Cidade:</label>
			    <input id="cp-cidade" type="text">
			</div>
			<div class="field four wide">
				<label>UF:</label>
			    <select id="cp-uf">
				    <option value="-1">- selecione -</option>
				    <option value="AC">AC</option>
				    <option value="AL">AL</option>
				    <option value="AM">AM</option>
				    <option value="AP">AP</option>
				    <option value="BA">BA</option>
				    <option value="CE">CE</option>
				    <option value="DF">DF</option>
				    <option value="ES">ES</option>
				    <option value="GO">GO</option>
				    <option value="MA">MA</option>
				    <option value="MG">MG</option>
				    <option value="MS">MS</option>
				    <option value="MT">MT</option>
				    <option value="PA">PA</option>
				    <option value="PB">PB</option>
				    <option value="PE">PE</option>
				    <option value="PI">PI</option>
				    <option value="PR">PR</option>
				    <option value="RJ">RJ</option>
				    <option value="RN">RN</option>
				    <option value="RO">RO</option>
				    <option value="RR">RR</option>
				    <option value="RS">RS</option>
				    <option value="SC">SC</option>
				    <option value="SE">SE</option>
				    <option value="SP">SP</option>
				    <option value="TO">TO</option>
				</select>
			</div>
		</div>
		<div class="two fields">
			<div class="field nine wide">
				<label>Telefone:</label>
				<input id="cp-telefone" type="text">
			</div>
			<div class="field four wide">
				<label>Complemento</label>
				<input id="cp-tel_complemento" type="text">
			</div>
		</div>
		<div class="two fields">
			<div class="field nine wide">
				<label>Celular</label>
				<input id="cp-celular" type="text">
			</div>
		</div>
	</div>
	<h4 class="ui dividing header">Dados de atendimento</h4>
	<div class="field nine wide">
		<label>Modalidade:</label>
	    <select id="cp-modalidade">
	    	<option value="-1">- selecione -</option>
	    	<option value="consulta">Consulta</option>
	    	<option value="exame">Exame</option>
	    </select>
	</div>
	<div class="field wide">
		<label>M�dico:</label>
	    <select id="cp-id_medico">
	    	<option value="-1">- selecione -</option>

			<?php
			foreach ($dados_medico as $key => $value) 
				{
				print '<option value="'.$value['id'].'">'.$value['medico'].'</option>';
				}
			?>

	    </select>
	</div>
	<div class="field wide">
		<label>Descri��o da indica��o do tratamento:</label>
	    <textarea id="cp-observacao"></textarea>
	</div>
	<div class="field nine wide">
		<label>Atendimento:</label>
	    <select id="cp-tipo_atendimento">
	    	<option value="-1">- selecione -</option>
	    	<option value="particular">Particular</option>
	    	<option value="convenio">Conv�nio</option>
	    </select>
	</div>
	<div class="field">
		<div class="two fields">
			<div class="field nine wide">
				<label>Conv�nio</label>
				<input id="cp-convenio_nome" type="text">
			</div>
			<div class="field six wide">
				<label>Plano</label>
				<input id="cp-convenio_plano" type="text">
			</div>
		</div>
	</div>
	<div class="field">
		<div class="two fields">
			<div class="field nine wide">
				<label>Data desejada para a consulta ou exame</label>
				<input id="cp-data_agendamento" type="date">
			</div>
			<div class="field six wide">
				<label>Per�odo</label>
				<select id="cp-periodo_agendamento">
					<option value="-1">- selecione -</option>
					<option value="manha">Manh�</option>
					<option value="tarde">Tarde</option>
				</select>
			</div>
		</div>
	</div>
	<div class="field">
		<p>Este cadastro � um pr�-agendamento. � necessario aguardar o contato de nossa equipe sobre a disponibilidade de hor�rio e confirma��o de agendamento.</p>
	</div>
	<div class="field">
		<div id="btn_agendar" class="ui labeled icon button big green">
		  <i class="calendar icon"></i>
		  Enviar
		</div>
	</div>
</div>
<div class="content-center">
	<div class="pad-40 shadow">
		<p class="titulo-1 color pad-t-10">Contato</p>
		
		<div id="contato-form-end" class="hide">
			<div class="ui icon message">
			  <i class="check icon green circular"></i>
			  <div class="content">
			    <div class="header">
			      Mensagem enviada
			    </div>
			    <p>Sua mensagem foi enviada com sucesso!</p>
			  </div>
			</div>
		</div>

		<div id="contato-form">
			<form class="ui form">
				<div class="field">
					<label>Nome *</label>
					<input id="cp-nome" class="form-field form-field-obrigatorio" data-field="nome" type="text">
				</div>

				<div class="two fields">
					<div class="field">
						<label>Email *</label>
						<input id="cp-email" class="form-field form-field-obrigatorio" data-field="email" type="text">
					</div>
					<div class="field">
						<label>Telefone</label>
						<input id="cp-telefone" class="form-field" data-field="tel" type="text">
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Celular *</label>
						<input id="cp-celular" class="form-field form-field-obrigatorio" data-field="cel" type="text">
					</div>
					<div class="field">
						<label>Operadora</label>
						<input id="cp-operadora" class="form-field" data-field="cel_operadora" type="text">
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Cidade</label>
						<input id="cp-cidade" class="form-field" data-field="cidade" type="text">
					</div>
					<div class="field">
						<label>UF</label>
						<input id="cp-uf" class="form-field" data-field="uf" type="text">
					</div>
				</div>

				<div class="field">
					<label>Mensagem</label>
					<textarea id="cp-mensagem" class="form-field form-field-obrigatorio" data-field="texto"></textarea>
				</div>
				<div class="field">
					<div id="btn-contato-send" class="ui labeled icon button color">
						<i class="send icon"></i>
							Enviar
						</div>
					</div>
			</form>
		</div>
	</div>
</div>
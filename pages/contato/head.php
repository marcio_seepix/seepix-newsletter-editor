<?php

$url_atual 			= SERVER_PROTOCOL.SERVER_URL.'/contato';
$titulo_meta 		= SYSTEM_TITTLE.' - Contato';
$descricao_pagina 	= 'Clínica paulista de alergia';
$st_palavras_chave 	= 'Fale conosco, Contato, são Paulo, SP, Alegia, medicina, Clínica';

print '<meta property="og:type" content="website">';
print '<meta property="og:site_name" content="'.$titulo_meta.'">';
print '<meta property="og:url" content="'.$url_atual.'">';
print '<meta property="og:title" content="'.$titulo_meta.'">';
print '<meta property="og:description" content="'.$descricao_pagina.'">';
print '<meta property="og:image" content="'.SERVER_PROTOCOL.SERVER_URL.'/img/compartilhar.jpg"> ';
print '<meta name="description" content="'.$descricao_pagina.'">';
print '<meta name="keywords" content="'.$st_palavras_chave.'">';
print '<title>'.$titulo_meta.'</title>';
?>

<link rel="stylesheet" type="text/css" href="components/message/css/message.css">
<script type="text/javascript" src="assets/js/functions/createToken.js"></script>
<script type="text/javascript" src="components/message/js/message.js"></script>

<style>
@media (max-width: 480px) {
	#content{
		margin-top: 120px;
		}
	}
@media (min-width: 481px) and (max-width: 840px) {
	#content{
		margin-top: 60px;
		}
	}
</style>
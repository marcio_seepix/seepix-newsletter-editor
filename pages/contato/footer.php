<script>

/* ---------------------------------
Actions
--------------------------------- */
$(document).on("click","#btn-contato-send",function()
	{
		contato_insere();
	});	

	
function contato_insere()
	{

		var nome = $("#cp-nome").val();
		var email = $("#cp-email").val();
		var celular = $("#cp-celular").val();
		var mensagem = $("#cp-mensagem").val();

		if(nome=='' || email=='' || celular=='' || mensagem=='')
			{
				msg = new message();
				msg.add('<i class="icon ban red"></i> Preencha os campos obrigatórios');	
				$(".form-field-obrigatorio").parent().addClass('error');

				return false;
			} else {
				$(".form-field-obrigatorio").parent().removeClass('error');
			}

		var url = 'api/contato_insere.php';
		var campos = '';

		$.each($(".form-field"),function(key,value)
			{
		   		var field = $(this).attr('data-field');
		   		var valor = $(this).val();

		   		campos+=field+'='+escape(valor)+'&';

		 	});

		$("#contato-form-end").removeClass('hide');
		$("#contato-form").addClass('hide');

		msg = new message();
		msg.add('<i class="icon check green"></i> Mensagem enviada com sucesso!');

		$.post(url,campos,function(lista)
			{

				var status = lista.status;
				if(status==1)
					{
						msg = new message();
						msg.classPosition('t-c');
						msg.add('<i class="icon check circle green"></i> '+lista.message);

						$(".form-field-obrigatorio").parent().removeClass('error');

						setTimeout(function(){
							loading_out();
							}, 2000);
					}

				if(status<1)
					{
						msg_error = new message();
						msg_error.classPosition('t-c');
						msg_error.add('<i class="icon warning circle red"></i> '+lista.message);

						$(".form-field-obrigatorio").parent().addClass('error');

					}

			},"json");

	}
</script>
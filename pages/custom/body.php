<?php
if($tag_page_load!='')
	{
		print $content;
	}

if((sizeof($pages_custom)>1) && $tag_page_load=='')
	{

		if(isset($titulo_pagina))
			{
				print '<p class="titulo-1 color pad-t-10">'.$titulo_pagina.'</p>';
			}

		print '<div class="pad-10">';
			foreach ($pages_custom as $key => $value)
				{
					
					$url_page = $pg_section.'/'.$value['tag'];

					print '<div class="grid margin-b-20">';
						print '<div class="grid-4">';
							print '<a href="'.$url_page.'">';
								print '<div class="aspect-wide bg-cover rounded" style="background-image:url('.$value['meta_image'].')">';
									print '<div></div>';
								print '</div>';
							print '</a>';
						print '</div>';
						print '<div class="grid-8">';
							print '<div class="pad-l-20">';
								print '<h1 class="font-size-5"><a href="'.$url_page.'" class="color bold">'.$value['meta_title'].'</a></h1>';
								print '<p>'.$value['meta_description'].'</p>';
							print '</div>';
						print '</div>';
					print '</div>';

				}
			print '</div>';
	}
?>
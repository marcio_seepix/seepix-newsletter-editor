<?php
$meta_titulo = '';
$meta_texto = '';
$meta_url = '';
$meta_img = '';
$meta_keywords = '';

if(isset($page_content))
	{
		$meta_titulo   = $page_content[0]['meta_title'];
		$meta_texto    = $page_content[0]['meta_description'];
		$meta_keywords = $page_content[0]['meta_tags'];
		$meta_img      = $page_content[0]['meta_image'];
		$meta_url      = SERVER_PROTOCOL.SERVER_URL.SERVER_DIRECTORY.$_SERVER['REQUEST_URI'];

		print '<meta property="og:type" content="website">';
		print '<meta property="og:site_name" content="'.$meta_titulo.'">';
		print '<meta property="og:url" content="'.$meta_url.'">';
		print '<meta property="og:title" content="'.$meta_titulo.'">';
		print '<meta property="og:description" content="'.$meta_texto.'">';
		print '<meta property="og:image" content="'.$meta_img.'"> ';
		print '<meta name="description" content="'.$meta_texto.'">';
		print '<meta name="keywords" content="'.$meta_keywords.'">';
		print '<title>'.$meta_titulo.'</title>';
		
	}

?>
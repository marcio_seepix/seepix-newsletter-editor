<?php
$content = '';
$tag_page_load = '';
$titulo_pagina = '';

if(isset($_GET['section_busca']))
	{
		$pg_section = get($_GET['section_busca']);
	}

if(isset($pg_section))
	{
		if($pg_section!='')
			{
				/* ---------------------------------
				Buscando as seções do site
				--------------------------------- */
				$db = new db;
				$secoes = $db->load("Select * from page_sections where ativo='1'");
		
				$secao_encontrada = 0;
				foreach ($secoes as $key => $value)
					{
						if($pg_section==$value['cod'])
							{
								$titulo_pagina = $value['titulo'];
								$secao_encontrada = 1;
							}
					}

				/* ---------------------------------
				Seção encontrada, buscando o ID
				--------------------------------- */
				if($secao_encontrada==1)
					{

						$query_section = $db->load("Select id from page_sections where cod='$pg_section' and ativo='1' limit 0,1");
						
						/* ---------------------------------
						Seção encontrada, vamos buscar um id
						da última página para este idioma
						--------------------------------- */
						if(sizeof($query_section)>0)
							{
								$id_section = $query_section[0]['id'];
								$pages_custom = $db->load("Select * from page where id_section='$id_section' and ativo='1' order by id desc");
								if(sizeof($pages_custom)==1)
									{
										$tag_page_load = $pages_custom[0]['tag'];
									}
							}

					}
				
			}
	}

if(isset($_GET['tag_busca']))
	{
		$tag_page_load = get($_GET['tag_busca']);
	}

if($tag_page_load!='')
	{
		$db = new db;
		$page_content = $db->load("Select * from page where tag='$tag_page_load'");
		$content = $page_content[0]['content'];
	}

?>